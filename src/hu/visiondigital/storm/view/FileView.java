package hu.visiondigital.storm.view;

import javafx.scene.layout.StackPane;

public class FileView extends BaseView {

    public FileView(StackPane root) {
        super("file", root);
    }

}