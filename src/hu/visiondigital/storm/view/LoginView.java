package hu.visiondigital.storm.view;

import java.io.*;
import javafx.scene.*;
import javafx.stage.*;
import java.util.logging.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import hu.visiondigital.storm.*;

/**
 *
 * @author juuci
 */
public class LoginView extends BaseView {

    private Stage stage;

    public LoginView() {
        super("login", new StackPane());
        initStage();
    }

    private void initStage() {
        stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(new Scene(ROOT, 250, 170));
        stage.setTitle(bundle.getString("label.title"));
        stage.getIcons().add(new Image(Storm.class.getResourceAsStream("/hu/visiondigital/storm/resources/img/icon-dark.png")));
    }

    @Override
    public void show() {
        try {
            loadView();
            initView();
            showAndWait();
        } catch (IOException ex) {
            System.err.println("[LoginView] : show()");
            Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void initView() {
        pane = (BorderPane) fxmlContent;
        setStylesheet(pane);
        ROOT.getChildren().clear();
        ROOT.getChildren().add(pane);
    }

    private void showAndWait() {
        stage.showAndWait();
    }

}
