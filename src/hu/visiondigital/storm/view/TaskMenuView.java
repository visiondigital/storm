package hu.visiondigital.storm.view;

import java.io.*;
import javafx.stage.*;
import javafx.scene.*;
import java.util.logging.*;
import javafx.application.*;
import javafx.beans.value.*;
import javafx.scene.layout.*;

/**
 *
 * @author belakede
 */
public final class TaskMenuView extends BaseView {

    private Stage stage;

    public TaskMenuView() {
        super("taskmenu", new StackPane());
        initStage();
        loadView();
    }

    private void initStage() {
        stage = new Stage(StageStyle.UNDECORATED);
        stage.setScene(new Scene(ROOT));
    }

    @Override
    protected void loadView() {
        try {
            super.loadView();
            initView();
        } catch (IOException ex) {
            System.err.println("[TaskMenuView] : loadView()");
            Logger.getLogger(TaskMenuView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void initView() {
        ROOT.getChildren().add((VBox) fxmlContent);
    }

    public void showAndHidePopup(final int x, final int y) {
        Platform.runLater(() -> {
            stage.setX(x);
            stage.setY(y);

            stage.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                if (!newValue) {
                    stage.close();
                }
            });

            stage.iconifiedProperty().addListener((ObservableValue<? extends Boolean> prop, Boolean oldValue, Boolean newValue) -> {
                if (newValue) {
                    stage.close();
                    stage.setIconified(false);
                }
            });

            stage.show();
        });
    }

    /**
     * Does nothing in TaskMenuView because showAndHidePopup() is responsible
     * for it.
     */
    @Override
    public void show() {
    }

}
