package hu.visiondigital.storm.view.interfaces;

import hu.visiondigital.storm.model.entity.*;

/**
 *
 * @author belakede
 */
public interface Uploadable<T extends EntityWithId> {

    public void upload(T entity);

}
