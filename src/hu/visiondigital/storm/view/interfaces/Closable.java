package hu.visiondigital.storm.view.interfaces;

/**
 * {@code Closable} summarizes the View classes that can be closed or hid.
 * @author juuci
 */
public interface Closable {
    
    /**
     * Close method to be implemented.
     */
    public abstract void close();
    
}
