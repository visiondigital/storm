package hu.visiondigital.storm.view;

import java.io.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import hu.visiondigital.storm.*;

/**
 *
 * @author juuci
 */
public class RegistrationView extends BaseView {

    private Stage stage;

    public RegistrationView() {
        super("registration", new StackPane());
        initStage();
    }

    private void initStage() {
        stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(new Scene(ROOT, 380, 230));
        stage.setTitle(bundle.getString("label.title"));
        stage.getIcons().add(new Image(Storm.class.getResourceAsStream("/hu/visiondigital/storm/resources/img/icon-dark.png")));
    }

    @Override
    public void show() {
        try {
            loadView();
            initView();
            showAndWait();
        } catch (IOException ex) {
            System.err.println("[RegistrationView] : show()");
            System.err.println(ex.getMessage());
        }
    }

    @Override
    protected void initView() {
        pane = (BorderPane) fxmlContent;
        setStylesheet(pane);
        ROOT.getChildren().clear();
        ROOT.getChildren().add(pane);
    }

    private void showAndWait() {
        stage.showAndWait();
    }

}
