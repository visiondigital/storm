package hu.visiondigital.storm.view;

import javafx.scene.layout.*;

public class PersonalView extends BaseView {
    
    public PersonalView(StackPane root) {
        super("personal", root);
    }

}