package hu.visiondigital.storm.view;

import java.io.*;
import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.scene.*;
import java.util.logging.*;
import javafx.application.*;
import javafx.beans.value.*;
import javafx.scene.layout.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.view.interfaces.*;

/**
 *
 * @author juuci
 */
public abstract class BaseView implements Closable {

    private final String VIEW;
    protected final Region REGION;
    protected final StackPane ROOT;

    protected Node fxmlContent;
    protected BorderPane pane;
    protected Locale locale;
    protected ResourceBundle bundle;

    public BaseView(String view, StackPane root) {
        this.VIEW = view;
        this.ROOT = root;
        this.REGION = new Region();
        initRegion();
        loadBundle();
    }

    private void initRegion() {
        REGION.getStyleClass().add("region");
    }

    protected void loadView() throws IOException {
        loadBundle();
        loadFxml();
    }

    private void loadBundle() {
        this.locale = SessionHelper.getInstance().getSettings().getLanguage().locale();
        this.bundle = ResourceBundle.getBundle("hu.visiondigital.storm.resources.i18n." + VIEW, locale);
    }

    private void loadFxml() throws IOException {
        URL resource = this.getClass().getResource("/hu/visiondigital/storm/resources/fxml/" + VIEW + ".fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setResources(bundle);
        fxmlContent = fxmlLoader.load(resource.openStream());
    }

    public void show() {
        try {
            loadView();
            initView();
        } catch (IOException ex) {
            System.err.println("[BaseView] : show()");
            Logger.getLogger(BaseView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void initView() {
        pane = (BorderPane) fxmlContent;
        setStylesheet(pane);
        Platform.runLater(() -> {
            if (!ROOT.getChildren().contains(pane)) {
                ROOT.getChildren().addAll(REGION, pane);
            }        
        });
    }

    @Override
    public void close() {
        Platform.runLater(() -> {
            ROOT.getChildren().remove(pane);
            ROOT.getChildren().remove(REGION);
        });
    }

    protected void setStylesheet(Parent node) {
        Platform.runLater(() -> {
            REGION.getStylesheets().add(SessionHelper.getInstance().getSettings().getTheme().filename());
            node.getStylesheets().add(SessionHelper.getInstance().getSettings().getTheme().filename());
            SessionHelper.getInstance().getSettings().themeProperty().addListener((ObservableValue<? extends Themes> observable, Themes oldValue, Themes newValue) -> {
                node.getStylesheets().removeAll(oldValue.filename());
                node.getStylesheets().add(newValue.filename());
                REGION.getStylesheets().removeAll(oldValue.filename());
                REGION.getStylesheets().add(newValue.filename());
            });        
        });
    }

}
