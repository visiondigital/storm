package hu.visiondigital.storm.view;

import java.io.*;
import javafx.stage.*;
import java.util.logging.*;
import javafx.scene.layout.*;
import javafx.beans.property.*;

/**
 *
 * @author juuci
 */
public class MainFrameView extends BaseView {

    private Stage stage;
    private final BooleanProperty hidden = new SimpleBooleanProperty(false);

    public MainFrameView(StackPane root) {
        super("mainframe", root);
        initStage();
    }

    private void initStage() {
        stage = (Stage) ROOT.getScene().getWindow();
        stage.setTitle(bundle.getString("label.title"));
    }

    @Override
    public void show() {
        if (hidden.get()) {
            showView();
        } else {
            loadView();
        }
    }

    private void showView() {
        hidden.set(false);
        stage.show();
    }

    @Override
    protected void loadView() {
        try {
            super.loadView();
            initView();
        } catch (IOException ex) {
            System.err.println("[MainFrameView] : loadView()");
            Logger.getLogger(MainFrameView.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }

    @Override
    protected void initView() {
        pane = (BorderPane) fxmlContent;
        setStylesheet(pane);
        ROOT.getChildren().clear();
        ROOT.getChildren().add(pane);
    }

    @Override
    public void close() {
        hidden.set(true);
        stage.hide();
    }

}