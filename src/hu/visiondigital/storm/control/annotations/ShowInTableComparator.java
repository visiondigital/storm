package hu.visiondigital.storm.control.annotations;

import java.util.Comparator;

/**
 * Compares the indexes the ShowInTable annotation's columnIndex.
 * @author belakede
 * @see ShowInTable
 */
public class ShowInTableComparator implements Comparator<ShowInTable> {

    @Override
    public int compare(ShowInTable o1, ShowInTable o2) {
        return Integer.valueOf(o1.columnIndex()).compareTo(o2.columnIndex());
    }

}
