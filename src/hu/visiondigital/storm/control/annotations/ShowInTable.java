package hu.visiondigital.storm.control.annotations;

import java.lang.annotation.*;

/**
 * The {@code ShowInTable} annotation indicates the columns which are expected to be part of a table.
 * @author belakede
 */
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ShowInTable {

    /**
     * Specifies which column in row the annotated column should be.
     * @return int 
     */
    int columnIndex() default Integer.MAX_VALUE;

    /**
     * Specifies the column's name.
     * @return  String
     */
    String columnName();
    
    /**
     * Gets the preferred width of the column with the default value of 135.
     * @return int
     */
    int prefWidth() default 135;

}
