package hu.visiondigital.storm.control;

import javafx.scene.text.*;
import javafx.beans.value.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;

/**
 * Javafx button with FontIcon.
 * @author belakede
 * @see FontIcon
 * @see Button
 */
public class ButtonWithFontIcon extends Button {

    /**
     * The icon the class uses.
     */
    private final ObjectProperty<FontIcon> fontIcon = new SimpleObjectProperty<>();

    /**
     * Creates a ButtonWithFontIcon button with no content and performs {@link initFields()} and {@link hookupChangeListeners()}
     */
    public ButtonWithFontIcon() {
        initFields();
        hookupChangeListeners();
    }

    /**
     * Creates a ButtonWithFontIcon button with the content of <b>icon</b> and performs {@link initFields()} and {@link hookupChangeListeners()} 
     * @param icon 
     */
    public ButtonWithFontIcon(FontAwesome icon) {
        initFields();
        hookupChangeListeners();
        setIcon(icon);
    }

    /**
     * Creates a ButtonWithFontIcon button with the content of <b>icon</b> and <b>text</b> and performs {@link initFields()} and {@link hookupChangeListeners()}
     * @param icon
     * @param text 
     */
    public ButtonWithFontIcon(FontAwesome icon, String text) {
        super(text);
        initFields();
        hookupChangeListeners();
        setIcon(icon);
    }

    /**
     * Gets the value of property fontIcon.
     * @return FontIcon
     */
    public FontIcon getFontIcon() {
        return fontIconProperty().get();
    }

    /**
     * Sets the value of property fontIcon.
     * @param fontIcon 
     */
    public void setFontIcon(FontIcon fontIcon) {
        fontIconProperty().set(fontIcon);
    }

    /**
     * The icon the class uses.
     * @return ObjectProperty<FontIcon>
     */
    public ObjectProperty<FontIcon> fontIconProperty() {
        return fontIcon;
    }

    /**
     * Gets the value property icon.
     * @return FontAwesome
     */
    public final FontAwesome getIcon() {
        return iconProperty().get();
    }
    
    /**
     * Sets the value of property icon passed through the parameter.
     * @param icon 
     */
    public final void setIcon(FontAwesome icon) {
        iconProperty().set(icon);
        setIconSize(getIconSize() - 1);
        setIconSize(getIconSize() + 1);
    }

    public final ObjectProperty<FontAwesome> iconProperty() {
        return fontIcon.get().iconProperty();
    }

    public final double getIconSize() {
        return iconSizeProperty().get();
    }

    public final void setIconSize(double iconSize) {
        iconSizeProperty().set(iconSize);
    }

    public final DoubleProperty iconSizeProperty() {
        return fontIcon.get().iconSizeProperty();
    }

    public final Paint getIconFill() {
        return iconFillProperty().get();
    }

    public final void setIconFill(Paint paint) {
        iconFillProperty().set(paint);
    }

    public final ObjectProperty<Paint> iconFillProperty() {
        return fontIconProperty().get().iconFillProperty();
    }

    private void initFields() {
        fontIcon.set(new FontIcon());
        setGraphic(fontIcon.get());
        getStyleClass().add("button-with-font-icon");
    }

    private void hookupChangeListeners() {
        fontIconProperty().addListener((ObservableValue<? extends FontIcon> ov, FontIcon t, FontIcon t1) -> {
            fontIcon.get().setFont(new Font(iconSizeProperty().doubleValue()));
            setGraphic(fontIcon.get());
        });
        iconProperty().addListener((ObservableValue<? extends FontAwesome> ov, FontAwesome t, FontAwesome t1) -> {
            fontIconProperty().get().setFont(new Font(iconSizeProperty().doubleValue()));
            setGraphic(fontIcon.get());
        });
    }

}
