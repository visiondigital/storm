package hu.visiondigital.storm.control;

import javafx.scene.text.*;
import javafx.beans.value.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;

/**
 * Javafx RadioButton with FontIcon.
 * @author belakede
 * @see FontIcon
 * @see RadioButton
 */
public class RadioButtonWithFontIcon extends RadioButton {

    private final ObjectProperty<FontIcon> fontIcon = new SimpleObjectProperty<>();

    public RadioButtonWithFontIcon() {
        initFields();
        hookupChangeListeners();
    }

    public RadioButtonWithFontIcon(FontAwesome icon) {
        initFields();
        hookupChangeListeners();
        setIcon(icon);
    }

    public RadioButtonWithFontIcon(FontAwesome icon, String string) {
        super(string);
        initFields();
        hookupChangeListeners();
        setIcon(icon);
    }

    public FontIcon getFontIcon() {
        return fontIconProperty().get();
    }

    public void setFontIcon(FontIcon fontIcon) {
        fontIconProperty().set(fontIcon);
    }

    public ObjectProperty<FontIcon> fontIconProperty() {
        return fontIcon;
    }

    public final FontAwesome getIcon() {
        return iconProperty().get();
    }

    public final void setIcon(FontAwesome icon) {
        iconProperty().set(icon);
        setIconSize(getIconSize() - 1);
        setIconSize(getIconSize() + 1);
    }

    public final ObjectProperty<FontAwesome> iconProperty() {
        return fontIcon.get().iconProperty();
    }

    public final double getIconSize() {
        return iconSizeProperty().get();
    }

    public final void setIconSize(double iconSize) {
        iconSizeProperty().set(iconSize);
    }

    public final DoubleProperty iconSizeProperty() {
        return fontIcon.get().iconSizeProperty();
    }

    public final Paint getIconFill() {
        return iconFillProperty().get();
    }

    public final void setIconFill(Paint paint) {
        iconFillProperty().set(paint);
    }

    public final ObjectProperty<Paint> iconFillProperty() {
        return fontIconProperty().get().iconFillProperty();
    }

    private void initFields() {
        fontIcon.set(new FontIcon());
        setGraphic(fontIcon.get());
        getStyleClass().add("radio-button-with-font-icon");
    }

    private void hookupChangeListeners() {
        fontIconProperty().addListener((ObservableValue<? extends FontIcon> ov, FontIcon t, FontIcon t1) -> {
            fontIcon.get().setFont(new Font(iconSizeProperty().doubleValue()));
            setGraphic(fontIcon.get());
        });
        iconProperty().addListener((ObservableValue<? extends FontAwesome> ov, FontAwesome t, FontAwesome t1) -> {
            fontIconProperty().get().setFont(new Font(iconSizeProperty().doubleValue()));
            setGraphic(fontIcon.get());
        });
    }

}
