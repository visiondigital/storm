package hu.visiondigital.storm.control;

import javafx.beans.value.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;

/**
 * Javafx Label with FontIcon.
 * @author belakede
 * @see FontIcon
 * @see Label
 */
public class LabelWithFontIcon extends Label {

    private final ObjectProperty<FontIcon> fontIcon = new SimpleObjectProperty<>(new FontIcon());

    public LabelWithFontIcon() {
        setGraphic(fontIcon.get());
        getStyleClass().add("label-with-font-icon");
        hookupChangeListeners();
    }

    public LabelWithFontIcon(FontAwesome icon) {
        setIcon(icon);
        setGraphic(fontIcon.get());
        getStyleClass().add("label-with-font-icon");
        hookupChangeListeners();
    }

    public LabelWithFontIcon(FontAwesome icon, String text) {
        super(text);
        setIcon(icon);
        setGraphic(fontIcon.get());
        getStyleClass().add("label-with-font-icon");
        hookupChangeListeners();
    }

    public FontIcon getFontIcon() {
        return fontIconProperty().get();
    }

    public void setFontIcon(FontIcon fontIcon) {
        fontIconProperty().set(fontIcon);
    }

    public ObjectProperty<FontIcon> fontIconProperty() {
        return fontIcon;
    }

    public final FontAwesome getIcon() {
        return iconProperty().get();
    }

    public final void setIcon(FontAwesome icon) {
        iconProperty().set(icon);
        setIconSize(getIconSize() - 1);
        setIconSize(getIconSize() + 1);
    }

    public final ObjectProperty<FontAwesome> iconProperty() {
        return fontIcon.get().iconProperty();
    }

    public final double getIconSize() {
        return iconSizeProperty().get();
    }

    public final void setIconSize(double iconSize) {
        iconSizeProperty().set(iconSize);
    }

    public final DoubleProperty iconSizeProperty() {
        return fontIcon.get().iconSizeProperty();
    }

    public final Paint getIconFill() {
        return iconFillProperty().get();
    }

    public final void setIconFill(Paint paint) {
        iconFillProperty().set(paint);
    }

    public final ObjectProperty<Paint> iconFillProperty() {
        return fontIconProperty().get().iconFillProperty();
    }

    private void hookupChangeListeners() {
        fontIconProperty().addListener((ObservableValue<? extends FontIcon> ov, FontIcon t, FontIcon t1) -> {
            setGraphic(fontIcon.get());
        });
    }

}
