package hu.visiondigital.storm.control;

import javafx.scene.text.*;
import javafx.scene.paint.*;
import javafx.beans.value.*;
import javafx.scene.control.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;

/**
 * Label with FontAwesome icon
 *
 * @author belakede
 */
public class FontIcon extends Label {

    /**
     * FontAwesome icon
     *
     * @see FontAwesome
     * {
     * @ link }
     */
    private final ObjectProperty<FontAwesome> icon = new SimpleObjectProperty<>();
    /**
     * The size of the icon.
     */
    private final DoubleProperty iconSize = new SimpleDoubleProperty(14);
    /**
     * The color of the icon.
     */
    private final ObjectProperty<Paint> iconColor = new SimpleObjectProperty<>();

    /**
     * Creates a FontIcon label with no content and performs
     * {@link hookupChangeListeners()} and {@link initFields()}
     */
    public FontIcon() {
        this(FontAwesome.NULL);
    }

    /**
     * Creates a Fonticon label with a FontAwesome icon passed through
     * <b>icon</b>
     *
     * @param icon
     */
    public FontIcon(FontAwesome icon) {
        hookupChangeListeners();
        initFields(icon);
    }

    /**
     * Gets the value of property icon.
     *
     * @return FontAwesome
     */
    public final FontAwesome getIcon() {
        return iconProperty().get();
    }

    /**
     * Sets the value of the property icon.
     *
     * @param icon
     */
    public final void setIcon(FontAwesome icon) {
        iconProperty().set(icon);
    }

    /**
     * The label with FontAwesome icon.
     *
     * @return ObjectProperty<FontAwesome>
     */
    public final ObjectProperty<FontAwesome> iconProperty() {
        return icon;
    }

    /**
     * Gets the value of property iconSize.
     *
     * @return double
     */
    public final double getIconSize() {
        return iconSizeProperty().get();
    }

    /**
     * Sets the value of property iconSize
     *
     * @param iconSize
     */
    public final void setIconSize(double iconSize) {
        iconSizeProperty().set(iconSize);
    }

    /**
     * The size of the icon.
     *
     * @return DoubleProperty
     */
    public final DoubleProperty iconSizeProperty() {
        return iconSize;
    }

    /**
     * Gets the color which fills the icon.
     *
     * @return Paint
     * @see Paint
     */
    public final Paint getIconFill() {
        return iconFillProperty().get();
    }

    /**
     * Sets the color the ion should be filled with.
     *
     * @param paint
     * @see Paint
     */
    public final void setIconFill(Paint paint) {
        iconFillProperty().set(paint);
    }

    /**
     * The color the icon is filled with.
     *
     * @return ObjectProperty<Paint>
     */
    public final ObjectProperty<Paint> iconFillProperty() {
        return iconColor;
    }

    /**
     * Initializes properties. Loads font and size of the icon.
     */
    private void initFields(FontAwesome icon) {
        iconProperty().set(icon);
        iconSizeProperty().set(14);
        Font.loadFont(FontAwesome.getFontSource(), iconSize.get());
        getStyleClass().addAll("font-icon", "font-awesome");
        getStylesheets().add(FontAwesome.getCssFile());
    }

    /**
     * Listens and reacts to changes performed on icons.
     */
    private void hookupChangeListeners() {
        iconFillProperty().addListener((ObservableValue<? extends Paint> ov, Paint t, Paint t1) -> {
            textFillProperty().set(t1);
        });

        iconProperty().addListener((ObservableValue<? extends FontAwesome> ov, FontAwesome t, FontAwesome t1) -> {
            textProperty().set(t1.get());
        });

        // disable changing text
        textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            textProperty().set(iconProperty().get().get());
        });
        // disable text fill
        textFillProperty().addListener((ObservableValue<? extends Paint> ov, Paint t, Paint t1) -> {
            if (t == null) {
                textFillProperty().set(Color.rgb(0, 0, 0, 0.0));
            }
        });
    }

}
