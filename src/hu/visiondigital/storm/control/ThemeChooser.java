package hu.visiondigital.storm.control;

import javafx.util.*;
import javafx.collections.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;

/**
 * Creates a chooser object for choosing a theme for the application. 
 * @author belakede
 * @see ComboBox
 * @see Themes
 */
public class ThemeChooser extends ComboBox<Themes> {
    
    /**
     * Creates a ThemeChooser with the values of the enum Themes.
     */
    public ThemeChooser() {
        this(FXCollections.observableArrayList(Themes.values()));
    }

    /**
     * Creates a ThemeChooser with the items passed through the parameter and initializes cells with <b>initCell()</b>
     * @param items 
     */
    private ThemeChooser(ObservableList<Themes> items) {
        super(items);
        initCell();
    }
    
    /**
     * Fills up the chooser object with the cells initialized in static class ColorRectCell.
     * @see ColorRectCell
     */
    private void initCell() {
        Callback<ListView<Themes>, ListCell<Themes>> factory = (ListView<Themes> list) -> new ColorRectCell();
        setCellFactory(factory);
        setButtonCell(factory.call(null));        
    }

    /**
     * A static class which creates a 130 wide and 18 high rectangle and fills with the theme's color.
     */
    static class ColorRectCell extends ListCell<Themes> {
        @Override
        public void updateItem(Themes item, boolean empty) {
            super.updateItem(item, empty);
            Rectangle rect = new Rectangle(130, 18);
            if (item != null) {
                rect.setFill(Color.web(item.color()));
                setGraphic(rect);
            }
        }
    }
    

}
