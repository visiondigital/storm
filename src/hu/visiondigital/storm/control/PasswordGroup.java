package hu.visiondigital.storm.control;

import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.helpers.*;

/**
 * Creates a password group with buttons for generating passwords and making passwords visible.
 * @author belakede
 */
public class PasswordGroup extends HBox {

    /**
     * Button with icon to make passwords visible.
     */
    private final ObjectProperty<ButtonWithFontIcon> show = new SimpleObjectProperty<>();
    /**
     * Passwordfield for password.
     */
    private final ObjectProperty<PasswordField> password = new SimpleObjectProperty<>();
    /**
     * Textfield for textual representation of password.
     */
    private final ObjectProperty<TextField> passwordText = new SimpleObjectProperty<>();
    /**
     * Button for generating passwords.
     */
    private final ObjectProperty<ButtonWithFontIcon> generator = new SimpleObjectProperty<>();
    /**
     * Shows if password should be hidden.
     */
    private final BooleanProperty hidden = new SimpleBooleanProperty(true);
    /**
     * Shows if password group should contain generator button.
     */
    private final BooleanProperty withoutGenerator = new SimpleBooleanProperty(false);
    
    /**
     * An Eventhandler handling the action performed on button <b>show</b>
     */
    private EventHandler showEvent;
    /**
     * An Eventhandler handling the action performed on button <b>generator</b>
     */
    private EventHandler generatorEvent;

    /**
     * Creates a PasswordGroup with no content and initializes the appearance.
     */
    public PasswordGroup() {
        hookupEventHandlers();
        
        initLayout();
        initShowField();
        initPasswordFields();
        initGenerateField();
        initChildren();
    }

    /**
     * Initializes passwordGroup layout.
     */
    private void initLayout() {
        setAlignment(Pos.CENTER_LEFT);
        setPadding(new Insets(0));
        setSpacing(0);
        getStylesheets().add("/hu/visiondigital/storm/resources/css/password-group.css");
        getStyleClass().add("password-group");
    }
    
    /**
     * Initializes button <b>show</b>.
     */
    private void initShowField() {
        ButtonWithFontIcon showButton = new ButtonWithFontIcon(FontAwesome.EYE);
        showButton.getStyleClass().add("password-group-left");
        showButton.setOnAction(showEvent);
        show.set(showButton);
    }
    
    /**
     * Initializes passwordfield and binds with the value of property <b>hidden</b>.
     */
    private void initPasswordFields() {
        PasswordField passwordField = new PasswordField();
        passwordField.getStyleClass().add("password-group-middle");
        password.set(passwordField);
        
        TextField passwordTextField = new TextField();
        passwordTextField.getStyleClass().add("password-group-middle");
        passwordTextField.setManaged(false);
        passwordTextField.setVisible(false);
        passwordText.set(passwordTextField);
        
        passwordTextField.managedProperty().bind(hidden.not());
        passwordTextField.visibleProperty().bind(hidden.not());
        
        passwordField.managedProperty().bind(hidden);
        passwordField.visibleProperty().bind(hidden);
        
        passwordTextField.textProperty().bindBidirectional(passwordField.textProperty());
    }
    
    /**
     * Initializes generator button and binds with the value of property <b>withoutGenerator</b>
     */
    private void initGenerateField() {
        ButtonWithFontIcon generatorButton = new ButtonWithFontIcon(FontAwesome.KEY);
        generatorButton.getStyleClass().add("password-group-right");
        generatorButton.setOnAction(generatorEvent);
        
        generatorButton.managedProperty().bind(withoutGenerator.not());
        generatorButton.visibleProperty().bind(withoutGenerator.not());
        generatorButton.disableProperty().bind(withoutGenerator);
        
        generator.set(generatorButton);
    }
    
    /**
     * Initializes elements of the passwordgroup and adds to the parent pane.
     */
    private void initChildren() {
        getChildren().addAll(show.get(), password.get(), passwordText.get(), generator.get());
    }

    /**
     * Links the eventhandlers with the buttons of the passwordgroup.
     */
    private void hookupEventHandlers() {
        showEvent = (EventHandler) (Event event) -> {
            if (show.get().getIcon().equals(FontAwesome.EYE)) {
                show.get().setIcon(FontAwesome.EYE_SLASH);
                hidden.set(false);
            } else {
                show.get().setIcon(FontAwesome.EYE);
                hidden.set(true);
            }
        };
        generatorEvent = (EventHandler) (Event event) -> {
            password.get().setText(PasswordHelper.generate());
        };
    }
    
    /**
     * Property for password.
     * @return StringProperty
     */
    public StringProperty textProperty() {
        return password.get().textProperty();
    }
    
    /**
     * Gets the value of the property text.
     * @return String
     */
    public String getText() {
        return textProperty().get();
    }
    
    /**
     * Sets the value of the property text.
     * @param value 
     */
    public void setText(String value) {
        textProperty().set(value);
    }
    
    /**
     * Decides if passwordfield should contain generator button.
     * @return 
     */
    public BooleanProperty withoutGeneratorProperty() {
        return withoutGenerator;
    }
    
    /**
     * Gets the value of the property withoutGenerator
     * @return boolean
     */
    public boolean getWithoutGenerator() {
        return withoutGenerator.get();
    }
    
    /**
     * Sets the value of property withoutGenerator.
     * @param value 
     */
    public void setWithoutGenerator(boolean value) {
        withoutGenerator.set(value);
    }

}
