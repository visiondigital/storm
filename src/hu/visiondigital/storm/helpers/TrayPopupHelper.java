package hu.visiondigital.storm.helpers;

import java.awt.*;
import java.net.*;
import javax.swing.*;
import javafx.stage.*;
import java.awt.event.*;
import javafx.application.*;
import hu.visiondigital.storm.*;
import hu.visiondigital.storm.view.*;
import hu.visiondigital.storm.controller.*;

/**
 * Creates a TrayPopup with a TaskMenu.
 * @author belakede
 * @see TrayPopup
 * @see TaskMenu
 */
public class TrayPopupHelper {

    private static TrayPopupHelper instance;

    public static TrayPopupHelper getInstance(Stage stage) {
        synchronized (TrayPopupHelper.class) {
            if (instance == null) {
                instance = new TrayPopupHelper(stage);
            }
            return instance;
        }
    }
    
    private final TaskMenuView taskMenu;
    
    private final Stage stage;
    private Image image;
    private TrayIcon trayIcon;
    
    private TrayPopupHelper(Stage stage) {
        this.stage = stage;
        this.taskMenu = new TaskMenuView();
    }

    public void createTrayIcon() {
        if (SystemTray.isSupported()) {
            loadIcon();
            initTrayIcon();
        }
    }

    private void loadIcon() {
        image = null;
        URL url = Storm.class.getResource("/hu/visiondigital/storm/resources/img/icon-dark.png");
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        image = toolkit.getImage(url);
    }

    private void initTrayIcon() {
        SystemTray tray = SystemTray.getSystemTray();
        trayIcon = new TrayIcon(image, "");
        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    taskMenu.showAndHidePopup(e.getXOnScreen(), e.getYOnScreen());
                } else if (SwingUtilities.isLeftMouseButton(e)) {
                    showAndHideStage();
                }
            }
        });

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.err.println(e);
        }
    }

    private void showAndHideStage() {
        FrontController front = FrontController.getInstance(null, null);
        Platform.runLater(() -> {
            if (SystemTray.isSupported()) {
                if (stage.isShowing()) {
                    front.closeRequest("MAINFRAME");
                } else {
                    front.dispatchRequest("MAINFRAME");
                }
            } else {
                System.exit(0);
            }
        });
    }
}
