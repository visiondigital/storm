package hu.visiondigital.storm.helpers;

import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.*;

/**
 * Singleton class that implements the encryption and decryption of passwords.
 * @author belakede
 */
public class EncryptionHelper {

    /**
     * The instance of the class.
     */
    private static EncryptionHelper instance;

    /**
     * Return the instance of this Singleton class if it is null.
     * @return EncryptionHelper
     */
    public static EncryptionHelper getInstance() {
        if (instance == null) {
            instance = new EncryptionHelper();
        }
        return instance;
    }

    /**
     * A key that is used for encryption and decryption.
     */
    private final String key = "ow!1O3'g1!3b'15a";
    /**
     * @see Key
     */
    private final Key aesKey;
    /**
     * @see Cipher
     */
    private Cipher cipher;

    /**
     * Creates an EncryptionHelper with no content and initializes its fields.
     */
    private EncryptionHelper() {
        aesKey = new SecretKeySpec(key.getBytes(), "AES");
        try {
            cipher = Cipher.getInstance("AES");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     * Encrypts the password passed through the parameter.
     * @param password
     * @return byte[]
     * @throws Exception 
     */
    public byte[] encrypt(String password) throws Exception {
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);
        return cipher.doFinal(password.getBytes());
    }

    /**
     * Decrypts the password passed through the parameter.
     * @param password
     * @return String
     * @throws Exception 
     */
    public String decrypt(byte[] password) throws Exception {
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        return new String(cipher.doFinal(password));
    }

    /**
     * @param password
     * @return String
     */
    public String hash(String password) {
        byte byteData[] = new byte[0];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byteData = md.digest();   
        } catch (NoSuchAlgorithmException ex) {
            System.err.println(ex.getMessage());
        }
        
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
