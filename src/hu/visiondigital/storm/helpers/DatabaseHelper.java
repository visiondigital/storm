package hu.visiondigital.storm.helpers;

import hu.visiondigital.storm.model.*;
import hu.visiondigital.storm.model.entity.*;

/**
 * Singleton class that returns the requested category's DatabaseManager
 *
 * @see DatabaseManager
 * @author belakede
 */
public class DatabaseHelper {

    private static DatabaseHelper instance;

    public static DatabaseHelper getInstance() {
        if (instance == null) {
            instance = new DatabaseHelper();
        }
        return instance;
    }

    private DatabaseHelper() {
    }

    public Manageable<BankAccount> getBankAccountManager() {
        return (Manageable<BankAccount>) ThreadWrapper.wrap(new DatabaseManager<>(BankAccount.class));
    }

    public Manageable<BankCard> getBankCardManager() {
        return (Manageable<BankCard>) ThreadWrapper.wrap(new DatabaseManager<>(BankCard.class));
    }

    public Manageable<Email> getEmailManager() {
        return (Manageable<Email>) ThreadWrapper.wrap(new DatabaseManager<>(Email.class));
    }

    public Manageable<File> getFileManager() {
        return (Manageable<File>) ThreadWrapper.wrap(new DatabaseManager<>(File.class));
    }

    public Manageable<Netbank> getNetbankManager() {
        return (Manageable<Netbank>) ThreadWrapper.wrap(new DatabaseManager<>(Netbank.class));
    }

    public Manageable<Other> getOtherManager() {
        return (Manageable<Other>) ThreadWrapper.wrap(new DatabaseManager<>(Other.class));
    }

    public Manageable<Personal> getPersonalManager() {
        return (Manageable<Personal>) ThreadWrapper.wrap(new DatabaseManager<>(Personal.class));
    }

    public Manageable<Phone> getPhoneManager() {
        return (Manageable<Phone>) ThreadWrapper.wrap(new DatabaseManager<>(Phone.class));
    }

    public Manageable<Settings> getSettingsManager() {
        return (Manageable<Settings>) ThreadWrapper.wrap(new DatabaseManager<>(Settings.class));
    }

    public Manageable<Website> getWebsiteManager() {
        return (Manageable<Website>) ThreadWrapper.wrap(new DatabaseManager<>(Website.class));
    }

    public Manageable getManager(String entity) {
        switch (entity.toUpperCase()) {
            case "BANKACCOUNT":
                return getBankAccountManager();
            case "BANKCARD":
                return getBankCardManager();
            case "EMAIL":
                return getEmailManager();
            case "FILE":
                return getFileManager();
            case "NETBANK":
                return getNetbankManager();
            case "OTHER":
                return getOtherManager();
            case "PERSONAL":
                return getPersonalManager();
            case "PHONE":
                return getPhoneManager();
            case "SETTINGS":
                return getSettingsManager();
            case "WEBSITE":
                return getWebsiteManager();
            default:
                return null;
        }
    }

    public Manageable getManager(EntityWithId entity) {
        if (entity instanceof BankAccount) {
            return getBankAccountManager();
        } else if (entity instanceof BankCard) {
            return getBankCardManager();
        } else if (entity instanceof Email) {
            return getEmailManager();
        } else if (entity instanceof File) {
            return getFileManager();
        } else if (entity instanceof Netbank) {
            return getNetbankManager();
        } else if (entity instanceof Other) {
            return getOtherManager();
        } else if (entity instanceof Personal) {
            return getPersonalManager();
        } else if (entity instanceof Phone) {
            return getPhoneManager();
        } else if (entity instanceof Settings) {
            return getSettingsManager();
        } else if (entity instanceof Website) {
            return getWebsiteManager();
        } else {
            return null;
        }
    }

}
