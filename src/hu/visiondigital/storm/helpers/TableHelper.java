package hu.visiondigital.storm.helpers;

import java.util.*;
import java.beans.*;
import javafx.event.*;
import java.lang.reflect.*;
import javafx.collections.*;
import javafx.beans.value.*;
import javafx.beans.binding.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.model.*;
import hu.visiondigital.storm.controller.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.control.annotations.*;

/**
 * Creates a TableView with the elements of the Entity passed through the type
 * parameter.
 *
 * @param <T>
 * @see TableView
 * @author belakede
 */
public class TableHelper<T extends EntityWithId> implements PropertyChangeListener {

    private final static String PATH = "hu.visiondigital.storm.resources.i18n.";

    public static <T extends EntityWithId> void setupTable(TableView table, T entity, String view) {
        getInstance(table, entity, view).setup();
    }

    @Deprecated
    public static <T extends EntityWithId> void addColumnsToTable(TableView table, T empty, String view) {
        setupTable(table, empty, view);
    }

    private static TableHelper instance;

    private static <T extends EntityWithId> TableHelper getInstance(TableView table, T entity, String view) {
        if (instance == null) {
            instance = new TableHelper(table);
        }
        instance.setBundle(view);
        instance.setEntity(entity);
        return instance;
    }

    private ResourceBundle bundle;

    private final TableView table;
    private T entity;
    private Class<T> type;
    private ObservableList<ShowInTable> columns;
    private ObservableMap<ShowInTable, String> properties;

    private TableHelper(TableView table) {
        this.table = table;
        register();
        hookupChangeListeners();
    }

    private void register() {
        GlobalPropertyChangeSupport.getInstance().unregisterPropertyChageListener(this);
        GlobalPropertyChangeSupport.getInstance().registerPropertyChageListener(this);
    }

    private void setEntity(T entity) {
        this.entity = entity;
        DatabaseHelper.getInstance().getManager(entity).getType();
    }

    private void setBundle(String view) {
        bundle = ResourceBundle.getBundle(PATH + view, SessionHelper.getInstance().getSettings().getLanguage().locale());
    }

    private TableView getTable() {
        return table;
    }

    private void hookupChangeListeners() {
        table.getSelectionModel().clearSelection();
        SessionHelper.getInstance().setValue("selected", null);
        table.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (newValue == null) {
                SessionHelper.getInstance().setValue("selected", null);
            } else {
                SessionHelper.getInstance().setValue("selected", (EntityWithId) newValue);
            }
        });
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event != null && event.getSource() instanceof Manageable && "getType".equals(event.getPropertyName())) {
            type = (Class<T>) event.getNewValue();
            DatabaseHelper.getInstance().getManager(entity).readAll();
        } else if (event != null && type != null && event.getSource() instanceof Manageable && "readAll".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(type)) {
                table.setItems(FXCollections.observableArrayList((List<T>) event.getNewValue()));
            }
        } else if (event != null && type != null && event.getSource() instanceof Manageable && "delete".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(type)) {
                table.getItems().remove(event.getNewValue());
            }
        } else if (event != null && type != null && event.getSource() instanceof Manageable && "create".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(type)) {
                table.getItems().add((T) event.getNewValue());
            }
        }
    }

    private void setup() {
        initColumns();
        createColumns();
        addContextMenu();
    }

    private void initColumns() {
        columns = FXCollections.observableArrayList();
        properties = FXCollections.observableHashMap();
        Method[] methods = entity.getClass().getMethods();
        for (Method method : methods) {
            ShowInTable annos = method.getAnnotation(ShowInTable.class);
            if (annos != null) {
                columns.add(annos);
                properties.put(annos, getProperyName(method.getName()));
            }
        }
        FXCollections.sort(columns, new ShowInTableComparator());
    }

    private String getProperyName(String value) {
        return Introspector.decapitalize(value.substring(3));
    }

    private void createColumns() {
        table.getColumns().clear();
        columns.stream().forEach((column) -> {
            TableColumn<T, String> tableColumn = new TableColumn<>(bundle.getString(column.columnName()));
            tableColumn.setPrefWidth(column.prefWidth());
            tableColumn.setCellValueFactory(new PropertyValueFactory<>(properties.get(column)));
            table.getColumns().add(tableColumn);
        });
    }

    private void addContextMenu() {
        table.setRowFactory((Object param) -> {
            final TableRow<T> row = new TableRow<>();
            row.contextMenuProperty().bind(
                    Bindings.when(Bindings.isNotNull(row.itemProperty()))
                    .then(createContextMenu())
                    .otherwise((ContextMenu) null));
            return row;
        });
    }

    private ContextMenu createContextMenu() {
        final ResourceBundle baseBundle = ResourceBundle.getBundle("hu.visiondigital.storm.resources.i18n.base", SessionHelper.getInstance().getSettings().getLanguage().locale());
        final ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(createEditMenuItem(baseBundle), createRemoveMenuItem(baseBundle));
        return menu;
    }

    private MenuItem createEditMenuItem(ResourceBundle baseBundle) {
        MenuItem editItem = new MenuItem(baseBundle.getString("menu.edit"));
        editItem.setOnAction((ActionEvent event) -> {
            T selectedItem = (T) table.getSelectionModel().getSelectedItem();
            SessionHelper.getInstance().setValue("selected", selectedItem);
            String categoryType = ((Categories) SessionHelper.getInstance().getValue("type")).name();
            FrontController controller = FrontController.getInstance(null, null);
            controller.dispatchRequest(categoryType);
        });
        return editItem;
    }

    private MenuItem createRemoveMenuItem(ResourceBundle baseBundle) {
        MenuItem removeItem = new MenuItem(baseBundle.getString("menu.delete"));
        removeItem.setOnAction((ActionEvent event) -> {
            int selectedIndex = table.getSelectionModel().getSelectedIndex();
            T selectedItem = (T) table.getSelectionModel().getSelectedItem();
            String categoryType = ((Categories) SessionHelper.getInstance().getValue("type")).name().replace("_", "");
            DatabaseHelper.getInstance().getManager(categoryType).delete(selectedItem);
            table.getItems().remove(selectedIndex);
        });
        return removeItem;
    }

}
