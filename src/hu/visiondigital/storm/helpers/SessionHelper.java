package hu.visiondigital.storm.helpers;

import java.util.*;
import java.beans.*;
import javafx.application.*;
import javafx.beans.value.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.model.*;
import hu.visiondigital.storm.model.entity.*;

/**
 * Logs in the user and handles variables without instantiation.
 *
 * @author belakede
 */
public class SessionHelper implements PropertyChangeListener {

    private static SessionHelper instance;

    public static SessionHelper getInstance() {
        if (instance == null) {
            instance = new SessionHelper();
        }
        return instance;
    }

    private final Map<String, Object> values = new HashMap<>();
    private final BooleanProperty loggedIn = new SimpleBooleanProperty(false);
    private final BooleanProperty firstRun = new SimpleBooleanProperty(true);
    private final ObjectProperty<Settings> settings = new SimpleObjectProperty<>(new Settings());
    private final ObjectProperty<Personal> personal = new SimpleObjectProperty<>(new Personal());
    private final ObjectProperty<Timer> timer = new SimpleObjectProperty<>(new Timer());

    private SessionHelper() {
        register();
        loadPersonal();
        loadSettings();
        hookupChangeListeners();
    }

    private void register() {
        GlobalPropertyChangeSupport.getInstance().registerPropertyChageListener(this);
    }

    private void loadPersonal() {
        DatabaseHelper.getInstance().getPersonalManager().readAll();
    }

    private void loadSettings() {
        DatabaseHelper.getInstance().getSettingsManager().readAll();
    }

    public boolean isFirstRun() {
        return firstRun.get();
    }

    public BooleanProperty firstRunProperty() {
        return firstRun;
    }

    public Object getValue(String key) {
        return (key != null && values.containsKey(key)) ? values.get(key) : null;
    }

    public void setValue(String key, Object value) {
        values.put(key, value);
    }

    public Personal getPersonal() {
        return personal.get();
    }

    public Settings getSettings() {
        return settings.get();
    }

    public Boolean isLoggedIn() {
        return loggedIn.get();
    }

    public BooleanProperty loggedInProperty() {
        return loggedIn;
    }

    public void register(Settings settings, Personal personal) {
        DatabaseHelper.getInstance().getSettingsManager().create(settings);
        DatabaseHelper.getInstance().getPersonalManager().create(personal);
    }

    private void hookupChangeListeners() {
        firstRun.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                loadPersonal();
                loadSettings();
            }
        });
        loggedIn.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                startTimer();
            }
        });
    }

    private void startTimer() {
        timer.get().cancel();
        timer.set(new Timer());
        timer.get().schedule(new TimerTask() {
            @Override
            public void run() {
                loggedIn.set(false);
            }
        }, settings.get().getPinLimit() * 60000);
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event != null && event.getSource() instanceof Manageable && "readAll".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(Personal.class)) {
                setPersonal(event.getNewValue());
            } else if (manager.getType().equals(Settings.class)) {
                setSettings(event.getNewValue());
            }
        } else if (event != null && event.getSource() instanceof Manageable && "create".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(Personal.class)) {
                personal.set((Personal) event.getNewValue());
                loggedInProperty().set(true);
            } else if (manager.getType().equals(Settings.class)) {
                settings.set((Settings) event.getNewValue());
                firstRunProperty().set(false);
            }
        } else if (event != null && event.getSource() instanceof Manageable && "update".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(Settings.class)) {
                startTimer();
            } 
        } else if (event != null && event.getSource() instanceof Manageable && "read".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(Personal.class)) {
                Platform.runLater(() -> {
                    personal.get().setNickname(((Personal) event.getNewValue()).getNickname());
                });
            }
        }
    }

    private void setPersonal(Object newValue) {
        List<Personal> personalList = (List<Personal>) newValue;
        if (!personalList.isEmpty()) {
            personal.set(personalList.get(0));
            firstRun.set(false);
        } else {
            firstRun.set(true);
        }
    }

    private void setSettings(Object newValue) {
        List<Settings> settingsList = (List<Settings>) newValue;
        if (!settingsList.isEmpty()) {
            settings.set(settingsList.get(0));
            firstRun.set(false);
        } else {
            firstRun.set(true);
        }
    }

}
