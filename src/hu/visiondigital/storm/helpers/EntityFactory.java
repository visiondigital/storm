package hu.visiondigital.storm.helpers;

import java.beans.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.model.*;
import hu.visiondigital.storm.model.entity.*;

/**
 * Singleton class that reads the specified entity's database representation and
 * gets its password.
 *
 * @author belakede
 */
public class EntityFactory implements PropertyChangeListener {

    /**
     * The instance of the class.
     */
    private static EntityFactory instance;

    /**
     * Return the instance of this Singleton class if it is null.
     *
     * @return EntityFactory
     */
    public static EntityFactory getInstance() {
        if (instance == null) {
            instance = new EntityFactory();
        }
        return instance;
    }

    private StringProperty password;
    
    /**
     * Creates an EntityFactory with no content.
     */
    private EntityFactory() {
        register();
        init();
    }

    private void register() {
        GlobalPropertyChangeSupport.getInstance().registerPropertyChageListener(this);
    }

    private void init() {
        password = new SimpleStringProperty();
    }
    
    public EntityFactory bindProperty(StringProperty value) {
        value.bind(password);
        return this;
    }
    
    /**
     * Copy the value of the selected EntityWithId's password to clipboard.
     *
     * @param category category
     * @param entity the selected entity
     */
    public void copyEntityPassword(Categories category, EntityWithId entity) {
        int id = entity.getId();
        if (category.equals(Categories.BANK_ACCOUNT)) {
            DatabaseHelper.getInstance().getBankAccountManager().read(id);
        } else if (category.equals(Categories.BANK_CARD)) {
            DatabaseHelper.getInstance().getBankCardManager().read(id);
        } else if (category.equals(Categories.EMAIL)) {
            DatabaseHelper.getInstance().getEmailManager().read(id);
        } else if (category.equals(Categories.FILE)) {
            DatabaseHelper.getInstance().getFileManager().read(id);
        } else if (category.equals(Categories.NETBANK)) {
            DatabaseHelper.getInstance().getNetbankManager().read(id);
        } else if (category.equals(Categories.OTHER)) {
            DatabaseHelper.getInstance().getOtherManager().read(id);
        } else if (category.equals(Categories.PHONE)) {
            DatabaseHelper.getInstance().getPhoneManager().read(id);
        } else if (category.equals(Categories.WEBSITE)) {
            DatabaseHelper.getInstance().getWebsiteManager().read(id);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event != null && event.getSource() instanceof Manageable && "read".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            password.set(loadEntityPassword(manager.getType(), event.getNewValue()));
        }
    }

    private String loadEntityPassword(Class type, Object newValue) {
        String value = "";
        if (type.equals(BankAccount.class)) {
            value = ((BankAccount) newValue).getSecurityCode();
        } else if (type.equals(BankCard.class)) {
            value = ((BankCard) newValue).getVerificationCode();
        } else if (type.equals(Email.class)) {
            value = ((Email) newValue).getPassword();
        } else if (type.equals(File.class)) {
            value = ((File) newValue).getPassword();
        } else if (type.equals(Netbank.class)) {
            value = ((Netbank) newValue).getPassword();
        } else if (type.equals(Other.class)) {
            value = ((Other) newValue).getPassword();
        } else if (type.equals(Phone.class)) {
            value = ((Phone) newValue).getPin();
        } else if (type.equals(Website.class)) {
            value = ((Website) newValue).getPassword();
        }
        return value;
    }

}
