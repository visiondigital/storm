package hu.visiondigital.storm.helpers;

import java.util.*;

/**
 * Generates a random password.
 * @author belakede
 */
public final class PasswordHelper {

    private final static String LOWER_CASE_CHARS = "abcdefghijklmnopqrstuvwxyz";
    private final static String UPPER_CASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final static String NUMERIC_CHARS = "0123456789";
    private final static String SPECIAL_CHARS = "+!%/=()|[]$<>#&@{},;.:-_";
    
    public static String generate() {
        Random random = new Random();
        int length = random.nextInt(8) + 8;
        List<Character> chars = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            chars.add(LOWER_CASE_CHARS.charAt(random.nextInt(LOWER_CASE_CHARS.length())));
            chars.add(UPPER_CASE_CHARS.charAt(random.nextInt(UPPER_CASE_CHARS.length())));
            chars.add(NUMERIC_CHARS.charAt(random.nextInt(NUMERIC_CHARS.length())));
            chars.add(SPECIAL_CHARS.charAt(random.nextInt(SPECIAL_CHARS.length())));
            Collections.shuffle(chars);
        }
        Collections.shuffle(chars);
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < length; i++) {
            password.append(chars.get(i));
        }
        return password.toString();
    }
    
}