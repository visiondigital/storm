package hu.visiondigital.storm.enums;

import java.util.*;
import hu.visiondigital.storm.helpers.*;

/**
 * Categories of Views with text to display on buttons.
 * @author belakede
 */
public enum Categories {
    
    /**
     * Bank Account category
     */
    BANK_ACCOUNT("button.bankaccount"),
    /**
     * Bank Card category
     */
    BANK_CARD("button.creditcard"),
    /**
     * Email category
     */
    EMAIL("button.emailaccount"), 
    /**
     * File category
     */
    FILE("button.files"), 
    /**
     * Netbank category
     */
    NETBANK("button.netbank"),
    /**
     * Other category
     */
    OTHER("button.other"),
    /**
     * Phone category
     */
    PHONE("button.phone"), 
    /**
     * Website category
     */
    WEBSITE("button.websites");
    
    private final String KEY;
    /**
     * Creates a Categories enum element with the content of <b>key</b>
     * @param key 
     */
    private Categories(String key) {
        KEY = key;
    }
    
    @Override
    public String toString() {
        ResourceBundle bundle = ResourceBundle.getBundle("hu.visiondigital.storm.resources.i18n.mainframe", SessionHelper.getInstance().getSettings().getLanguage().locale());
        return bundle.getString(KEY);
    }
    
}
