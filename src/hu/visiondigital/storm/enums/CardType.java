package hu.visiondigital.storm.enums;

/**
 * Bankcard types that can be used.
 * @author timea
 */
public enum CardType {

    /**
     * Visa card
     */
    VISA,
    /**
     * American Express card
     */
    AMERICAN_EXPRESS,
    /**
     * BC Card
     */
    BC_CARD,
    /**
     * MasterCard
     */
    MASTERCARD,
    /**
     * MasterCard Alaska
     */
    MASTERCARD_ALASKA,
    /**
     * MasterCard Canada
     */
    MASTERCARD_CANADA,
    /**
     * Carte Blanche
     */
    CARTE_BLANCHE,
    /**
     * Chine Union Pay card
     */
    CHINA_UNION_PAY,
    /**
     * Discover card
     */
    DISCOVER,
    /**
     * Diner's Club card
     */
    DINERS_CLUB,
    /**
     * Carta SI card
     */
    CARTA_SI,
    /**
     * Carte Bleue card
     */
    CARTE_BLEUE,
    /**
     * Dankort card
     */
    DANKORT,
    /**
     * Delta card
     */
    DELTA,
    /**
     * Electron card
     */
    ELECTRON,
    /**
     * Japan Credit Bureau card
     */
    JAPAN_CREDIT_BUREAU,
    /**
     * Maestro card
     */
    MAESTRO,
    /**
     * Switch card
     */
    SWITCH,
    /**
     * Solo card
     */
    SOLO;
    
}
