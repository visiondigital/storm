package hu.visiondigital.storm.enums;

/**
 * Themes that are available.
 * @author belakede
 */
public enum Themes {
    
    /**
     * Burnt Sienna theme
     */
    BURNT_SIENNA("Burnt sienna", "#E96B56"),
    /**
     * Cadet Blue theme
     */
    CADET_BLUE("Cadet blue", "#4DA79C"),
    /**
     * Davy's Grey theme
     */
    DAVY_S_GREY("Davy's grey", "#545454"),
    /**
     * Fandango theme
     */
    FANDANGO("Fandango", "#A94085"),
    /**
     * Medium Sea Green theme
     */
    MEDIUM_SEA_GREEN("Medium sea green", "#40AC60"),
    /**
     * Mulberry theme
     */
    MULBERRY("Mulberry", "#C44881"),
    /**
     * Teal Blue theme
     */
    TEAL_BLUE("Teal blue", "#397D91");
    
    private final static String STYLE_DIRECTORY = "/hu/visiondigital/storm/resources/css/";
    
    private final String NAME;
    private final String FILE_NAME;
    private final String CODE;
    
    /**
     * Creates a Themes enum element with the content of name and code
     * @param name
     * @param code 
     */
    private Themes(String name, String code) {
        this.NAME = name;
        this.CODE = code;
        this.FILE_NAME = STYLE_DIRECTORY.concat(name.replaceAll("[^\\w\\s]","-").replace(" ", "-").toLowerCase()).concat(".css");
    }
    
    @Override
    public String toString() {
        return NAME;
    }
    
    /**
     * Gets the path of the css file used for the theme.
     * @return String
     */
    public String filename() {
        return FILE_NAME;
    }
    
    /**
     * Gets the html code of the color used for the theme.
     * @return String
     */
    public String color() {
        return CODE;
    }
    
}
