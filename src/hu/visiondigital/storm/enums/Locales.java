package hu.visiondigital.storm.enums;

import java.util.*;

/**
 * Languages that are available.
 * @author belakede
 */
public enum Locales {
    
    /**
     * Hungarian
     */
    HU("Magyar", new Locale("hu", "hu"));
    
    private final String LABEL;
    private final Locale LOCALE;
    /**
     * Creates a Locales element with the contents of <b>label</b> and <b>locale</b>
     * @param label
     * @param locale 
     */
    private Locales(String label, Locale locale) {
        LABEL = label;
        LOCALE = locale;
    }
    
    @Override
    public String toString() {
        return LABEL;
    }
    
    /**
     * Creates a Locale with no content.
     * @return Locale
     */
    public Locale locale() {
        return LOCALE;
    }

}
