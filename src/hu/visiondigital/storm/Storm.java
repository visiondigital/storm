package hu.visiondigital.storm;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.image.*;
import javafx.application.*;
import javafx.scene.layout.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.controller.*;
import static javafx.application.Application.*;

/**
 *
 * @author belakede
 */
public class Storm extends Application {

    private StackPane root;

    public static void main(String[] args) {
        // starting database
        DatabaseHelper.getInstance();
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        setUserAgentStylesheet(STYLESHEET_MODENA);
        Platform.setImplicitExit(false);

        initStage(stage);
        startTray(stage);
        startMain(stage);
    }

    private void initStage(Stage stage) {
        stage.initStyle(StageStyle.UNDECORATED);
        root = new StackPane();
        stage.setScene(new Scene(root, 700, 540));
        stage.getIcons().add(new Image(Storm.class.getResourceAsStream("/hu/visiondigital/storm/resources/img/icon-dark.png")));
    }

    private void startTray(Stage stage) {
        if (SessionHelper.getInstance().getSettings().isShowOnSystray()) {
            TrayPopupHelper.getInstance(stage).createTrayIcon();
        }
    }

    private void startMain(Stage stage) {
        FrontController front = FrontController.getInstance(stage, root);
        front.dispatchRequest("MAINFRAME");
    }
}
