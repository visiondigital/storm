package hu.visiondigital.storm.model;

/**
 *
 * @author belakede
 */
public class ThreadWrapper {

    public static Object wrap(Object obj) {
        return ManagerProxy.newInstance(obj);
    }
}
