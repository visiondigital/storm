package hu.visiondigital.storm.model;

import java.util.*;
import hu.visiondigital.storm.model.entity.*;

/**
 * A class implements the {@code Email} interface to implement the CRUD methods of an entity's database representation.
 * @author timeanna
 * @param <T>
 * @see EntityWithId
 */
public interface Manageable<T extends EntityWithId> {

    /**
     * Gets all rows from the table.
     * @return List<>
     */
    public List<T> readAll();

    /**
     * Gets specified number of rows of the table from a specified index.
     * @param firstResult
     * @param maxResult
     * @return List
     */
    public List<T> readSome(int firstResult, int maxResult);

    /**
     * Gets the row of the table with the id specified in the parameter.
     * @param id
     * @return T
     */
    public T read(Integer id);

    /**
     * Creates a representation in the database from the entity passed through the parameter.
     * @param t
     * @return T
     */
    public T create(T t);

    /**
     * Updates the database representation of the entity passed through the parameter.
     * @param t
     */
    public T update(T t);

    /**
     * Deletes the database representation of the entity passed through the parameter.
     * @param t
     */
    public T delete(T t);

    /**
     * Gets the number of rows in the table.
     * @return int
     */
    public int rowCount();

    /**
     * Gets the class of the type parameter. 
     * @return Class<>
     */
    public Class<T> getType();
}
