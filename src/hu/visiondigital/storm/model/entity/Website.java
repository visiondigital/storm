package hu.visiondigital.storm.model.entity;

import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.control.annotations.*;

/**
 * The {@code Website} class creates an object from a row of the table Website.
 * @author timeanna
 */
@Entity(name = "WEBSITE")
public class Website implements Serializable, EntityWithId {

    private static final long serialVersionUID = 1L;

    /**
     * Creates a default Website entity with no content.
     */
    public Website() {
    }

    /**
     * The identifier of the website in the database.
     */
    private final IntegerProperty id = new SimpleIntegerProperty();
    /**
     * The name of the website.
     */
    private final StringProperty name = new SimpleStringProperty();
    /**
     * The url of the website.
     */
    private final StringProperty url = new SimpleStringProperty();
    /**
     * The username on the website.
     */
    private final StringProperty username = new SimpleStringProperty();
    /**
     * The email address connected to the website.
     */
    private final StringProperty email = new SimpleStringProperty();
    /**
     * The password for the website.
     */
    private final StringProperty password = new SimpleStringProperty();
    /**
     * The encrypted version of the password for the website.
     */
    private final ObjectProperty<byte[]> encryptedPassword = new SimpleObjectProperty<>();
    /**
     * The description provided to the website.
     */
    private final StringProperty description = new SimpleStringProperty();

    /**
     * Gets the value of the property id.
     * @return Integer
     * @see Id
     * @see Override
     * @see GeneratedValue
     */
    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    /**
     * Sets the value of the property id.
     * @param value 
     */
    public void setId(Integer value) {
        id.set(value);
    }

    /**
     * The identifier of the email in the database.
     * @return IntegerProperty
     */
    public IntegerProperty idProperty() {
        return id;
    }

    /**
     * Gets the value of the property name.
     * @return String
     * @see ShowInTable
     */
    @Column(name = "NAME")
    @ShowInTable(columnIndex = 0, columnName = "column.name")
    public String getName() {
        return name.get();
    }

    /**
     * Sets the value of the property name.
     * @param value 
     */
    public void setName(String value) {
        name.set(value);
    }

    /**
     * The name connected to the website.
     * @return StringProperty
     */
    public StringProperty nameProperty() {
        return name;
    }

    /**
     * Gets the value of the property URL.
     * @return String
     * @see ShowInTable
     */
    @Column(name = "URL")
    @ShowInTable(columnIndex = 1, columnName = "column.url")
    public String getUrl() {
        return url.get();
    }

    /**
     * Sets the value of the property URL.
     * @param value 
     */
    public void setUrl(String value) {
        url.set(value);
    }

    /**
     * The URL connected to the website.
     * @return StringProperty
     */
    public StringProperty urlProperty() {
        return url;
    }

    /**
     * Gets the value of the property username.
     * @return String
     * @see ShowInTable
     */
    @Column(name = "USERNAME")
    @ShowInTable(columnIndex = 2, columnName = "column.username")
    public String getUsername() {
        return username.get();
    }

    /**
     * Sets the value of the property username.
     * @param value 
     */
    public void setUsername(String value) {
        username.set(value);
    }

    /**
     * The username connected to the e-mail account.
     * @return StringProperty
     */
    public StringProperty usernameProperty() {
        return username;
    }

    /**
     * Gets the value of the property email address.
     * @return String
     * @see ShowInTable
     */
    @Column(name = "EMAIL")
    @ShowInTable(columnIndex = 2, columnName = "column.email")
    public String getEmail() {
        return email.get();
    }

    /**
     * Sets the value of the property email address.
     * @param value 
     */
    public void setEmail(String value) {
        email.set(value);
    }

    /**
     * The email address connected to the e-mail account.
     * @return StringProperty
     */
    public StringProperty emailProperty() {
        return email;
    }

    /**
     * Gets the value of the property encryptedPassword.
     * @return byte[]
     */
    @Column(name = "PASSWORD")
    public byte[] getEncryptedPassword() {
        return encryptedPassword.get();
    }

    /**
     * Sets the value of the property encryptedPassword.
     * @param value 
     */
    private void setEncryptedPassword(byte[] value) {
        encryptedPassword.set(value);
    }

    /**
     * Gets the value of the property password. Before returning the value {@link decryptIfNull} is performed
     * @return String
     */
    @Transient
    public String getPassword() {
        decryptIfNull();
        return password.get();
    }

    /**
     * Decrypts the encrypted field password and sets it as the value of the property password.
     * @see EncryptionHelper
     */
    private void decryptIfNull() {
        if (password.get() == null) {
            try {
                String decrypted = EncryptionHelper.getInstance().decrypt(encryptedPassword.get());
                password.set(decrypted);
            } catch (Exception ex) {
                System.err.println("ERROR: decryption");
            }
        }
    }

    /**
     * Sets the value of the property encryptedPassword and password.
     * @param value 
     */
    public void setPassword(String value) {
        try {
            byte[] encrypted = EncryptionHelper.getInstance().encrypt(value);
            encryptedPassword.set(encrypted);
            password.set(value);
        } catch (Exception ex) {
            System.err.println("ERROR: encryption");
        }
    }

    /**
     * The password for the e-mail account.
     * @return StringProperty
     */
    public StringProperty passwordProperty() {
        decryptIfNull();
        return password;
    }

    /**
     * Gets the value of the property description.
     * @return String 
     */
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description.get();
    }

    /**
     * Sets the value of the property description.
     * @param value 
     */
    public void setDescription(String value) {
        description.set(value);
    }

    /**
     * The description of the e-mail account.
     * @return StringProperty
     */
    public StringProperty descriptionProperty() {
        return description;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getUrl())
                .append(", ")
                .append(getEmail())
                .toString();
    }

}
