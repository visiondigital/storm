package hu.visiondigital.storm.model.entity;

/**
 * Groups together the entity classes.
 * @author timeanna
 */
public interface EntityWithId {

    /**
     * Gets the id of the entity's representation in the database.
     * @return Integer
     */
    public Integer getId();
}
