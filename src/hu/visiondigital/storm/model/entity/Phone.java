package hu.visiondigital.storm.model.entity;

import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.control.annotations.*;

/**
 *The {@code Phone} class creates an object from a row of the table Phone.
 * @author timeanna
 */
@Entity(name = "PHONE")
public class Phone implements Serializable, EntityWithId {
    private static final long serialVersionUID = 1L;

    /**
     * The identifier of the phone in the database.
     */
    private final IntegerProperty id = new SimpleIntegerProperty();
    /**
     * The number connected to the subscription.
     */
    private final StringProperty number = new SimpleStringProperty();
    /**
     * The pin code of the SIM card.
     */
    private final StringProperty pin = new SimpleStringProperty();
    /**
     * The puk code of the SIM card.
     */
    private final StringProperty puk = new SimpleStringProperty();
    /**
     * The identification number of the subscriber.
     */
    private final StringProperty securityCode = new SimpleStringProperty();
    /**
     * The provider of the phone subscription.
     */
    private final StringProperty provider = new SimpleStringProperty();

    /**
     * Creates a default Phone entity with no content.
     */
    public Phone() {
    }
    
    /**
     * Gets the value of the property id.
     * @return Integer
     * @see Id
     * @see Override
     * @see GeneratedValue
     */
    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    /**
     * Sets the value of the property id.
     * @param value 
     */
    public void setId(Integer value) {
        id.set(value);
    }

    /**
     * The identifier of the phone in the database.
     * @return IntegerProperty
     */
    public IntegerProperty idProperty() {
        return id;
    }

    /**
     * Gets the value of the property number.
     * @return String
     * @see ShowInTable
     */
    @Column(name = "NUMBER")
    @ShowInTable(columnIndex = 0, columnName = "column.number")
    public String getNumber() {
        return number.get();
    }

    /**
     * Sets the value of the property number.
     * @param value 
     */
    public void setNumber(String value) {
        number.set(value);
    }

    /**
     * The number connected to the subscription.
     * @return StringProperty
     */
    public StringProperty numberProperty() {
        return number;
    }
    
    /**
     * Gets the value of the property provider.
     * @return String
     * @see ShowInTable
     */
    @Column(name = "PROVIDER")
    @ShowInTable(columnIndex = 1, columnName = "column.provider")
    public String getProvider() {
        return provider.get();
    }

    /**
     * Sets the value of the property provider.
     * @param value 
     */
    public void setProvider(String value) {
        provider.set(value);
    }

    /**
     * The provider of the subscription.
     * @return StringProperty
     */
    public StringProperty providerProperty() {
        return provider;
    }
    
    /**
     * Gets the value of the property pin.
     * @return String
     */
    @Column(name = "PIN")
    public String getPin() {
        return pin.get();
    }

    /**
     * Sets the value of the property pin.
     * @param value 
     */
    public void setPin(String value) {
        pin.set(value);
    }

    /**
     * The pin code of the SIM card.
     * @return StringProperty
     */
    public StringProperty pinProperty() {
        return pin;
    }

    /**
     * Gets the value of the property puk.
     * @return String
     */
    @Column(name = "PUK")
    public String getPuk() {
        return puk.get();
    }

    /**
     * Sets the value of the property puk.
     * @param value 
     */
    public void setPuk(String value) {
        puk.set(value);
    }

    /**
     * The puk code of the SIM card.
     * @return IntegerProperty
     */
    public StringProperty pukProperty() {
        return puk;
    }

    /**
     * Gets the value of the property securityCode.
     * @return String
     */
    @Column(name = "SECURITY_CODE")
    public String getSecurityCode() {
        return securityCode.get();
    }

    /**
     * Sets the value of the property securityCode.
     * @param value 
     */
    public void setSecurityCode(String value) {
        securityCode.set(value);
    }

    /**
     * The The identification number of the subscriber.
     * @return StringProperty
     */
    public StringProperty securityCodeProperty() {
        return securityCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getNumber())
                .toString();
    }

}
