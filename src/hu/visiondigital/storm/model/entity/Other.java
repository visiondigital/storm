package hu.visiondigital.storm.model.entity;

import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.control.annotations.*;

/**
 *
 * @author timeanna
 */
@Entity(name = "OTHER")
public class Other implements Serializable, EntityWithId {

    private static final long serialVersionUID = 1L;

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty password = new SimpleStringProperty();
    private final ObjectProperty<byte[]> encryptedPassword = new SimpleObjectProperty<>();
    private final StringProperty description = new SimpleStringProperty();
    private final StringProperty identifier = new SimpleStringProperty();

    public Other() {
    }

    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "NAME")
    @ShowInTable(columnIndex = 0, columnName = "column.name")
    public String getName() {
        return name.get();
    }

    public void setName(String value) {
        name.set(value);
    }

    public StringProperty nameProperty() {
        return name;
    }

    @Column(name = "PASSWORD")
    public byte[] getEncryptedPassword() {
        return encryptedPassword.get();
    }

    private void setEncryptedPassword(byte[] value) {
        encryptedPassword.set(value);
    }

    @Transient
    public String getPassword() {
        decryptIfNull();
        return password.get();
    }

    private void decryptIfNull() {
        if (password.get() == null) {
            try {
                String decrypted = EncryptionHelper.getInstance().decrypt(encryptedPassword.get());
                password.set(decrypted);
            } catch (Exception ex) {
                System.err.println("ERROR: decryption");
            }
        }
    }

    public void setPassword(String value) {
        try {
            byte[] encrypted = EncryptionHelper.getInstance().encrypt(value);
            encryptedPassword.set(encrypted);
            password.set(value);
        } catch (Exception ex) {
            System.err.println("ERROR: encryption");
        }
    }

    public StringProperty passwordProperty() {
        decryptIfNull();
        return password;
    }

    @Column(name = "DESCRIPTION")
    @ShowInTable(columnIndex = 1, columnName = "column.description")
    public String getDescription() {
        return description.get();
    }

    public void setDescription(String value) {
        description.set(value);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    @Column(name = "IDENTIFIER")
    @ShowInTable(columnIndex = 2, columnName = "column.identifier")
    public String getIdentifier() {
        return identifier.get();
    }

    public void setIdentifier(String value) {
        identifier.set(value);
    }

    public StringProperty identifierProperty() {
        return identifier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getName())
                .append(", ")
                .append(getIdentifier())
                .toString();
    }

}
