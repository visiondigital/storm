package hu.visiondigital.storm.model.entity;

import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.control.annotations.*;

/**
 *
 * @author timeanna
 */
@Entity(name = "NETBANK")
public class Netbank implements Serializable, EntityWithId {
    
    private static final long serialVersionUID = 1L;

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty username = new SimpleStringProperty();
    private final StringProperty password = new SimpleStringProperty();
    private final ObjectProperty<byte[]> encryptedPassword = new SimpleObjectProperty<>();
    private final StringProperty url = new SimpleStringProperty();
    private final StringProperty phone = new SimpleStringProperty();
    
    public Netbank() {
    }
    
    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "USERNAME")
    @ShowInTable(columnIndex = 0, columnName = "column.username")
    public String getUsername() {
        return username.get();
    }

    public void setUsername(String value) {
        username.set(value);
    }

    public StringProperty usernameProperty() {
        return username;
    }

    @Column(name = "PASSWORD")
    public byte[] getEncryptedPassword() {
        return encryptedPassword.get();
    }

    private void setEncryptedPassword(byte[] value) {
        encryptedPassword.set(value);
    }

    @Transient
    public String getPassword() {
        decryptIfNull();
        return password.get();
    }

    private void decryptIfNull() {
        if (password.get() == null) {
            try {
                String decrypted = EncryptionHelper.getInstance().decrypt(encryptedPassword.get());
                password.set(decrypted);
            } catch (Exception ex) {
                System.err.println("ERROR: decryption");
            }
        }
    }

    public void setPassword(String value) {
        try {
            byte[] encrypted = EncryptionHelper.getInstance().encrypt(value);
            encryptedPassword.set(encrypted);
            password.set(value);
        } catch (Exception ex) {
            System.err.println("ERROR: encryption");
        }
    }

    public StringProperty passwordProperty() {
        decryptIfNull();
        return password;
    }

    @Column(name = "URL")
    @ShowInTable(columnIndex = 1, columnName = "column.url")
    public String getUrl() {
        return url.get();
    }

    public void setUrl(String value) {
        url.set(value);
    }

    public StringProperty urlProperty() {
        return url;
    }

    @Column(name = "PHONE")
    @ShowInTable(columnIndex = 2, columnName = "column.phone")
    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String value) {
        phone.set(value);
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getUrl())
                .append(", ")
                .append(getUsername())
                .toString();
    }

}
