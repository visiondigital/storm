package hu.visiondigital.storm.model.entity;

import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.control.annotations.*;

/**
 *
 * @author timeanna
 */
@Entity(name = "FILE")
public class File implements Serializable, EntityWithId {

    private static final long serialVersionUID = 1L;

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty filename = new SimpleStringProperty();
    private final StringProperty md5sum = new SimpleStringProperty();
    private final StringProperty password = new SimpleStringProperty();
    private final ObjectProperty<byte[]> encryptedPassword = new SimpleObjectProperty<>();
    private final StringProperty description = new SimpleStringProperty();

    public File() {
    }

    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "FILENAME")
    @ShowInTable(columnIndex = 0, columnName = "column.file")
    public String getFilename() {
        return filename.get();
    }

    public void setFilename(String value) {
        filename.set(value);
    }

    public StringProperty filenameProperty() {
        return filename;
    }

    @Column(name = "MD5SUM")
    public String getMd5sum() {
        return md5sum.get();
    }

    public void setMd5sum(String value) {
        md5sum.set(value);
    }

    public StringProperty md5sumProperty() {
        return md5sum;
    }

    @Column(name = "PASSWORD")
    public byte[] getEncryptedPassword() {
        return encryptedPassword.get();
    }

    private void setEncryptedPassword(byte[] value) {
        encryptedPassword.set(value);
    }

    @Transient
    public String getPassword() {
        decryptIfNull();
        return password.get();
    }

    private void decryptIfNull() {
        if (password.get() == null) {
            try {
                String decrypted = EncryptionHelper.getInstance().decrypt(encryptedPassword.get());
                password.set(decrypted);
            } catch (Exception ex) {
                System.err.println("ERROR: decryption");
            }
        }
    }

    public void setPassword(String value) {
        try {
            byte[] encrypted = EncryptionHelper.getInstance().encrypt(value);
            encryptedPassword.set(encrypted);
            password.set(value);
        } catch (Exception ex) {
            System.err.println("ERROR: encryption");
        }
    }

    public StringProperty passwordProperty() {
        decryptIfNull();
        return password;
    }

    @Column(name = "DESCRIPTION")
    @ShowInTable(columnIndex = 1, columnName = "column.description")
    public String getDescription() {
        return description.get();
    }

    public void setDescription(String value) {
        description.set(value);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getFilename())
                .toString();
    }

}
