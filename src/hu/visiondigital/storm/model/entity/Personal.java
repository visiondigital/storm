package hu.visiondigital.storm.model.entity;

import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;

/**
 *
 * @author timeanna
 */
@Entity(name = "PERSONAL")
public class Personal implements Serializable, EntityWithId {
    
    private static final long serialVersionUID = 1L;
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty fullName = new SimpleStringProperty();
    private final StringProperty idCardNo = new SimpleStringProperty();
    private final StringProperty nationalIdNo = new SimpleStringProperty();
    private final StringProperty taxIdNo = new SimpleStringProperty();
    private final StringProperty socialSecurityNo = new SimpleStringProperty();
    private final StringProperty drivingLicenceNo = new SimpleStringProperty();
    private final StringProperty passportNo = new SimpleStringProperty();
    private final StringProperty studentCardNo = new SimpleStringProperty();
    private final StringProperty educationIdNo = new SimpleStringProperty();
    private final StringProperty nickname = new SimpleStringProperty();

    
    public Personal() {
    }

    
    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "FULL_NAME")
    public String getFullName() {
        return fullName.get();
    }

    public void setFullName(String value) {
        fullName.set(value);
    }

    public StringProperty fullNameProperty() {
        return fullName;
    }

    @Column(name = "NICKNAME")
    public String getNickname() {
        return nickname.get();
    }

    public void setNickname(String value) {
        nickname.set(value);
    }

    public StringProperty nicknameProperty() {
        return nickname;
    }

    @Column(name = "ID_CARD_NO")
    public String getIdCardNo() {
        return idCardNo.get();
    }

    public void setIdCardNo(String value) {
        idCardNo.set(value);
    }

    public StringProperty idCardNoProperty() {
        return idCardNo;
    }

    @Column(name = "NATIONAL_ID_NO")
    public String getNationalIdNo() {
        return nationalIdNo.get();
    }

    public void setNationalIdNo(String value) {
        nationalIdNo.set(value);
    }

    public StringProperty nationalIdNoProperty() {
        return nationalIdNo;
    }

    @Column(name = "TAX_ID_NO")
    public String getTaxIdNo() {
        return taxIdNo.get();
    }

    public void setTaxIdNo(String value) {
        taxIdNo.set(value);
    }

    public StringProperty taxIdNoProperty() {
        return taxIdNo;
    }

    @Column(name = "SOCIAL_SECURITY_NO")
    public String getSocialSecurityNo() {
        return socialSecurityNo.get();
    }

    public void setSocialSecurityNo(String value) {
        socialSecurityNo.set(value);
    }

    public StringProperty socialSecurityNoProperty() {
        return socialSecurityNo;
    }

    @Column(name = "DRIVING_LICENCE_NO")
    public String getDrivingLicenceNo() {
        return drivingLicenceNo.get();
    }

    public void setDrivingLicenceNo(String value) {
        drivingLicenceNo.set(value);
    }

    public StringProperty drivingLicenceNoProperty() {
        return drivingLicenceNo;
    }

    @Column(name = "PASSPORT_NO")
    public String getPassportNo() {
        return passportNo.get();
    }

    public void setPassportNo(String value) {
        passportNo.set(value);
    }

    public StringProperty passportNoProperty() {
        return passportNo;
    }

    @Column(name = "STUDENT_CARD_NO")
    public String getStudentCardNo() {
        return studentCardNo.get();
    }

    public void setStudentCardNo(String value) {
        studentCardNo.set(value);
    }

    public StringProperty studentCardNoProperty() {
        return studentCardNo;
    }

    @Column(name = "EDUCATION_ID_NO")
    public String getEducationIdNo() {
        return educationIdNo.get();
    }

    public void setEducationIdNo(String value) {
        educationIdNo.set(value);
    }

    public StringProperty educationIdNoProperty() {
        return educationIdNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getFullName();
    }
}
