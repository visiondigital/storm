package hu.visiondigital.storm.model.entity;

import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.helpers.*;

/**
 *
 * @author timeanna
 */
@Entity(name = "SETTINGS")
public class Settings implements Serializable, EntityWithId {
    
    private static final long serialVersionUID = 1L;
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty pinCode = new SimpleStringProperty("0000");
    private final ObjectProperty<Themes> theme = new SimpleObjectProperty<>(Themes.BURNT_SIENNA);
    private final ObjectProperty<Locales> language = new SimpleObjectProperty(Locales.HU);
    private final ObjectProperty<Categories> defaultCategory = new SimpleObjectProperty<>(Categories.FILE);
    private final IntegerProperty pinLimit = new SimpleIntegerProperty(10);
    private final BooleanProperty hideOnExit = new SimpleBooleanProperty(false);
    private final BooleanProperty showOnSystray = new SimpleBooleanProperty(true);
    
    public Settings() {
    
    }
    
    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "PIN_CODE")
    public String getPinCode() {
        return pinCode.get();
    }

    public void setPinCode(String value) {
        pinCode.set(value);
    }

    public StringProperty pinCodeProperty() {
        return pinCode;
    }

    @Column(name = "THEME")
    @Enumerated(EnumType.STRING)
    public Themes getTheme() {
        return theme.get();
    }

    public void setTheme(Themes value) {
        theme.set(value);
    }

    public ObjectProperty<Themes> themeProperty() {
        return theme;
    }

    @Column(name = "LANGUAGE")
    @Enumerated(EnumType.STRING)
    public Locales getLanguage() {
        return language.get();
    }

    public void setLanguage(Locales value) {
        language.set(value);
    }

    public ObjectProperty<Locales> languageProperty() {
        return language;
    }

    @Column(name = "DEFAULT_CATEGORY")
    @Enumerated(EnumType.STRING)
    public Categories getDefaultCategory() {
        return defaultCategory.get();
    }

    public void setDefaultCategory(Categories value) {
        defaultCategory.set(value);
    }

    public ObjectProperty<Categories> defaultCategoryProperty() {
        return defaultCategory;
    }

    @Column(name = "PIN_LIMIT")
    public int getPinLimit() {
        return pinLimit.get();
    }

    public void setPinLimit(int value) {
        pinLimit.set(value);
    }

    public IntegerProperty pinLimitProperty() {
        return pinLimit;
    }

    @Column(name = "HIDE_ON_EXIT")
    public boolean isHideOnExit() {
        return hideOnExit.get();
    }

    public void setHideOnExit(boolean value) {
        hideOnExit.set(value);
    }

    public BooleanProperty hideOnExitProperty() {
        return hideOnExit;
    }

    @Column(name = "SHOW_ON_SYSTRAY")
    public boolean isShowOnSystray() {
        return showOnSystray.get();
    }

    public void setShowOnSystray(boolean value) {
        showOnSystray.set(value);
    }

    public BooleanProperty showOnSystrayProperty() {
        return showOnSystray;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getPinCode())
                .append(getTheme())
                .append(getLanguage())
                .append(getDefaultCategory())
                .append(getPinLimit())
                .append(isHideOnExit())
                .append(isShowOnSystray())
                .toString();
    }
}
