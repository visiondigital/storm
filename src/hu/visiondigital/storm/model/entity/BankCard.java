package hu.visiondigital.storm.model.entity;

import hu.visiondigital.storm.control.annotations.ShowInTable;
import hu.visiondigital.storm.enums.CardType;
import java.io.*;
import javax.persistence.*;
import javafx.beans.property.*;

/**
 *
 * @author timeanna
 */
@Entity(name = "BANK_CARD")
public class BankCard implements Serializable, EntityWithId {

    private static final long serialVersionUID = 1L;

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty cardNumber = new SimpleStringProperty();
    private final StringProperty expirationDate = new SimpleStringProperty();
    private final StringProperty verificationCode = new SimpleStringProperty();
    private final StringProperty owner = new SimpleStringProperty();
    private final ObjectProperty<CardType> cardType = new SimpleObjectProperty();
    private final StringProperty disableNumber = new SimpleStringProperty();
    private final ObjectProperty<BankAccount> bankAccount = new SimpleObjectProperty();

    public BankCard() {
    }

    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @ShowInTable(columnIndex = 0, columnName = "column.cardnumber")
    @Column(name = "CARD_NUMBER")
    public String getCardNumber() {
        return cardNumber.get();
    }

    public void setCardNumber(String value) {
        cardNumber.set(value);
    }

    public StringProperty cardNumberProperty() {
        return cardNumber;
    }

    @ShowInTable(columnIndex = 1, columnName = "column.expirationdate")
    @Column(name = "EXPIRATION_DATE")
    public String getExpirationDate() {
        return expirationDate.get();
    }

    public void setExpirationDate(String value) {
        expirationDate.set(value);
    }

    public StringProperty expirationDateProperty() {
        return expirationDate;
    }

    @Column(name = "VERIFICATION_CODE")
    public String getVerificationCode() {
        return verificationCode.get();
    }

    public void setVerificationCode(String value) {
        verificationCode.set(value);
    }

    public StringProperty verificationCodeProperty() {
        return verificationCode;
    }

    @ShowInTable(columnIndex = 0, columnName = "column.owner")
    @Column(name = "OWNER")
    public String getOwner() {
        return owner.get();
    }

    public void setOwner(String value) {
        owner.set(value);
    }

    public StringProperty ownerProperty() {
        return owner;
    }

    @Column(name = "CARD_TYPE")
    @Enumerated(EnumType.STRING)
    @ShowInTable(columnIndex = 2, columnName = "column.cardtype")
    public CardType getCardType() {
        return cardType.get();
    }

    public void setCardType(CardType value) {
        cardType.set(value);
    }

    public ObjectProperty<CardType> cardTypeProperty() {
        return cardType;
    }

    @Column(name = "DISABLE_NUMBER")
    public String getDisableNumber() {
        return disableNumber.get();
    }

    public void setDisableNumber(String value) {
        disableNumber.set(value);
    }

    public StringProperty disableNumberProperty() {
        return disableNumber;
    }

    @ManyToOne
    @JoinColumn(name = "BANK_ACCOUNT_ID")
    @ShowInTable(columnIndex = 3, columnName = "column.accountnumber")
    public BankAccount getBankAccount() {
        return bankAccount.get();
    }

    public void setBankAccount(BankAccount value) {
        bankAccount.set(value);
    }

    public ObjectProperty bankAccountProperty() {
        return bankAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getCardNumber())
                .toString();
    }

}
