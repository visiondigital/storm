package hu.visiondigital.storm.model.entity;

import hu.visiondigital.storm.control.annotations.ShowInTable;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import javafx.collections.*;
import javafx.beans.property.*;

/**
 *
 * @author timeanna
 */
@Entity(name = "BANK_ACCOUNT")
public class BankAccount implements Serializable, EntityWithId {

    private static final long serialVersionUID = 1L;

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty owner = new SimpleStringProperty();
    private final StringProperty bank = new SimpleStringProperty();
    private final StringProperty accountNumber = new SimpleStringProperty();
    private final StringProperty iban = new SimpleStringProperty();
    private final StringProperty securityCode = new SimpleStringProperty();
    private final ListProperty<BankCard> bankCards = new SimpleListProperty<>();
    
    public BankAccount() {
    }

    @Id
    @Override
    @GeneratedValue
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "OWNER")
    @ShowInTable(columnIndex = 0, columnName = "column.owner")
    public String getOwner() {
        return owner.get();
    }

    public void setOwner(String value) {
        owner.set(value);
    }

    public StringProperty ownerProperty() {
        return owner;
    }

    
    @Column(name = "BANK")
    @ShowInTable(columnIndex = 1, columnName = "column.bank")
    public String getBank() {
        return bank.get();
    }

    public void setBank(String value) {
        bank.set(value);
    }

    public StringProperty bankProperty() {
        return bank;
    }

    @Column(name = "ACCOUNT_NUMBER")
    @ShowInTable(columnIndex = 2, columnName = "column.accountnumber")
    public String getAccountNumber() {
        return accountNumber.get();
    }

    public void setAccountNumber(String value) {
        accountNumber.set(value);
    }

    public StringProperty accountNumberProperty() {
        return accountNumber;
    }

    @ShowInTable(columnIndex = 3, columnName = "column.iban")
    @Column(name = "IBAN")
    public String getIban() {
        return iban.get();
    }

    public void setIban(String value) {
        iban.set(value);
    }

    public StringProperty ibanProperty() {
        return iban;
    }

    @Column(name = "SECURITY_CODE")
    public String getSecurityCode() {
        return securityCode.get();
    }

    public void setSecurityCode(String value) {
        securityCode.set(value);
    }

    public StringProperty securityCodeProperty() {
        return securityCode;
    }

    @OneToMany(mappedBy = "bankAccount")
    public List<BankCard> getBankCards() {
        return bankCards.get();
    }

    public void setBankCards(List<BankCard> value) {
        bankCards.set(FXCollections.observableArrayList(value));
    }

    public ListProperty bankCardsProperty() {
        return bankCards;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Website)) {
            return false;
        }
        Website other = (Website) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder(getBank()).append(", ").append(getOwner()).toString();
    }

}
