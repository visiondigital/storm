package hu.visiondigital.storm.model;

import java.util.*;
import javax.persistence.*;
import javax.persistence.criteria.*;
import hu.visiondigital.storm.model.entity.*;

/**
 *
 * @param <T>
 * @see Manageable
 * @see EntityWithId
 * @author timeanna
 */
public class DatabaseManager<T extends EntityWithId> implements Manageable<T> {

    /**
     * Instance of an EntityManagerFactory which creates an EntityManager
     */
    private final EntityManagerFactory emf;
    /**
     * The type of the class passed through the type parameter
     */
    private final Class<T> type;

    /**
     * Creates a DatabaseManager with the content provided in parameter
     * <b>type</b>.
     *
     * @param type
     */
    public DatabaseManager(Class<T> type) {
        this.type = type;
        this.emf = Persistence.createEntityManagerFactory("StormPU");
    }

    /**
     * Gets the EntityManager instance created by the EntityManagerFactory
     *
     * @return EntityManager
     * @see EntityManagerFactory
     */
    private EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * @param t
     * @see Manageable
     */
    @Override
    public T create(T t) {
        if (t != null) {
            EntityManager em = null;
            try {
                em = getEntityManager();
                em.getTransaction().begin();
                em.persist(t);
                em.getTransaction().commit();
            } catch (Exception ex) {
                return null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return t;
    }

    /**
     * @param t
     * @see Manageable
     */
    @Override
    public T update(T t) {
        if (t != null) {
            EntityManager em = null;
            try {
                em = getEntityManager();
                em.getTransaction().begin();
                t = em.merge(t);
                em.getTransaction().commit();
            } catch (Exception ex) {
                return null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return t;
    }

    /**
     * @param t
     * @see Manageable
     */
    @Override
    public T delete(T t) {
        if (t != null) {
            EntityManager em = null;
            try {
                em = getEntityManager();
                em.getTransaction().begin();
                try {
                    t = em.getReference(type, t.getId());
                    t.getId();
                } catch (EntityNotFoundException enfe) {
                    return null;
                }
                em.remove(t);
                em.getTransaction().commit();
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return t;
    }

    /**
     * @return List
     * @see Manageable
     */
    @Override
    public List<T> readAll() {
        return findEntities(true, -1, -1);
    }

    /**
     * @param maxResults
     * @param firstResult
     * @return List
     * @see Manageable
     */
    @Override
    public List<T> readSome(int maxResults, int firstResult
    ) {
        return findEntities(false, maxResults, firstResult);
    }

    /**
     * @param all
     * @param maxResults
     * @param firstResult
     * @return List
     * @see Manageable
     */
    private List<T> findEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(type));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * @param id
     * @return T
     * @see Manageable
     */
    @Override
    public T read(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(type, id);
        } finally {
            em.close();
        }
    }

    /**
     * @return int
     * @see Manageable
     */
    @Override
    public int rowCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<T> rt = cq.from(type);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    /**
     * @return Class
     * @see Manageable
     */
    @Override
    public Class<T> getType() {
        return type;
    }
}
