package hu.visiondigital.storm.model;

import java.beans.*;

/**
 *
 * @author belakede
 */
public class GlobalPropertyChangeSupport {

    private static GlobalPropertyChangeSupport instance;

    public static GlobalPropertyChangeSupport getInstance() {
        if (instance == null) {
            instance = new GlobalPropertyChangeSupport();
        }
        return instance;
    }

    private final PropertyChangeSupport propertyChangeSupport;

    private GlobalPropertyChangeSupport() {
        propertyChangeSupport = new PropertyChangeSupport(GlobalPropertyChangeSupport.class);
    }

    public void firePropertyChange(String property, Object oldValue, Object newValue) {
        propertyChangeSupport.firePropertyChange(property, oldValue, newValue);
    }

    public void firePropertyChange(Object source, String property, Object oldValue, Object newValue) {
        propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(source, property, oldValue, newValue));
    }

    public void registerPropertyChageListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void unregisterPropertyChageListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
}
