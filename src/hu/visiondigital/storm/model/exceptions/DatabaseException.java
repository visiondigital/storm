package hu.visiondigital.storm.model.exceptions;

/**
 * Thrown to indicate that the CRUD operation failed.
 * @author timeanna
 */
public class DatabaseException extends Exception {

    /**
     * Creates a new DatabaseException with no content.
     */
    public DatabaseException() {
    }

    /**
     * Creates a new DatabaseException with the content of msg.
     * @param msg 
     */
    public DatabaseException(String msg) {
        super(msg);
    }
}
