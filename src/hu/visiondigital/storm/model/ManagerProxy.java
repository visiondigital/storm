package hu.visiondigital.storm.model;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author belakede
 */
public class ManagerProxy implements InvocationHandler {

    private final Object object;

    public static Object newInstance(Object object) {
        return Proxy.newProxyInstance(object.getClass().getClassLoader(), object.getClass().getInterfaces(), new ManagerProxy(object));
    }

    private ManagerProxy(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, final Method method, final Object[] args) throws Throwable {
        new Thread(() -> {
            try {
                onResult(method.getName(), method.invoke(object, args));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(ManagerProxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
        return null;
    }

    private void onResult(String name, Object result) {
        GlobalPropertyChangeSupport.getInstance().firePropertyChange(object, name, null, result);
    }

}
