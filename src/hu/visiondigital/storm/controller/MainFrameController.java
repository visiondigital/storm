package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.util.*;
import javafx.event.*;
import javafx.animation.*;
import javafx.application.*;
import javafx.scene.input.*;
import javafx.beans.value.*;
import javafx.scene.control.*;
import javafx.beans.property.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;

/**
 *
 * @author juuci
 */
public class MainFrameController extends CategoryController implements Initializable {

    @FXML
    private Label title;
    @FXML
    private LabelWithFontIcon message;
    @FXML
    private Button user, showpassword;

    private final Clipboard clipboard = Clipboard.getSystemClipboard();

    @FXML
    private void closeButtonAction(ActionEvent event) {
        if (SessionHelper.getInstance().getSettings().isHideOnExit()) {
            FrontController controller = FrontController.getInstance(null, null);
            controller.closeRequest("MAINFRAME");
        } else {
            System.exit(0);
        }
    }

    public void addNewPassword() {
        SessionHelper.getInstance().setValue("selected", null);
        Categories type = (Categories) SessionHelper.getInstance().getValue("type");
        FrontController controller = FrontController.getInstance(null, null);
        controller.dispatchRequest(type.name());
    }

    public void copyPassword(ActionEvent event) {
        FrontController controller = FrontController.getInstance(null, null);
        if (controller.isAuthenticUser()) {
            Categories type = (Categories) SessionHelper.getInstance().getValue("type");
            EntityWithId value = (EntityWithId) SessionHelper.getInstance().getValue("selected");
            copyToClipboard(type, value);
            showMessage();
        }
    }

    private void copyToClipboard(Categories type, EntityWithId value) {
        StringProperty password = new SimpleStringProperty();
        password.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Platform.runLater(() -> {
                ClipboardContent content = new ClipboardContent();
                content.putString(newValue);
                clipboard.setContent(content);
            });
        });
        EntityFactory.getInstance().bindProperty(password).copyEntityPassword(type, value);
    }

    private void showMessage() {
        message.setVisible(true);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), (ActionEvent event) -> {
            message.setVisible(false);
        }));
        timeline.setDelay(Duration.seconds(10));
        timeline.play();
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        this.categories = new ArrayList<>();
        init();
    }

    @Override
    protected void init() {
        title.setText(bundle.getString("label.title"));
        message.setText(bundle.getString("label.copied"));
        user.setText(bundle.getString("button.user"));
        settings.setText(bundle.getString("button.settings"));
        files.setText(bundle.getString("button.files"));
        bankaccount.setText(bundle.getString("button.bankaccount"));
        creditcard.setText(bundle.getString("button.creditcard"));
        netbank.setText(bundle.getString("button.netbank"));
        phone.setText(bundle.getString("button.phone"));
        emailaccount.setText(bundle.getString("button.emailaccount"));
        websites.setText(bundle.getString("button.websites"));
        other.setText(bundle.getString("button.other"));
        newpassword.setText(bundle.getString("button.newpassword"));
        showpassword.setText(bundle.getString("button.showpassword"));
        quit.setText(bundle.getString("button.quit"));

        hookupChangeListeners();
        loadTableContent();
        Collections.addAll(categories, settings, files, bankaccount, creditcard, netbank, phone, emailaccount, websites, other);
    }

    private void loadTableContent() {
        Categories type = (Categories) SessionHelper.getInstance().getValue("type");
        if (type == null) {
            type = SessionHelper.getInstance().getSettings().getDefaultCategory();
        }
        load(type);
    }

    private void hookupChangeListeners() {
        showpassword.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
        user.textProperty().bind(SessionHelper.getInstance().getPersonal().nicknameProperty());
    }

}
