package hu.visiondigital.storm.controller;

import javafx.scene.layout.*;
import hu.visiondigital.storm.view.*;

/**
 *
 * @author juuci
 */
public class Dispatcher {

    private final MainFrameView mainFrameView;
    private final PersonalView personalView;
    private final SettingsView settingsView;
    private final BankAccountView bankAccountView;
    private final FileView fileView;
    private final BankCardView bankCardView;
    private final NetbankView netbankView;
    private final PhoneView phoneView;
    private final EmailView emailView;
    private final WebsiteView websiteView;
    private final OtherView otherView;

    public Dispatcher(StackPane root) {
        mainFrameView = new MainFrameView(root);
        personalView = new PersonalView(root);
        settingsView = new SettingsView(root);
        bankAccountView = new BankAccountView(root);
        fileView = new FileView(root);
        bankCardView = new BankCardView(root);
        netbankView = new NetbankView(root);
        phoneView = new PhoneView(root);
        emailView = new EmailView(root);
        websiteView = new WebsiteView(root);
        otherView = new OtherView(root);
    }

    public void dispatch(String request) {
        if (request.equalsIgnoreCase("MAINFRAME")) {
            mainFrameView.show();
        } else if (request.equalsIgnoreCase("PERSONAL_FORM")) {
            personalView.show();
        } else if (request.equalsIgnoreCase("SETTINGS_FORM")) {
            settingsView.show();
        } else if (request.equalsIgnoreCase("BANK_ACCOUNT")) {
            bankAccountView.show();
        } else if (request.equalsIgnoreCase("FILE")) {
            fileView.show();
        } else if (request.equalsIgnoreCase("BANK_CARD")) {
            bankCardView.show();
        } else if (request.equalsIgnoreCase("NETBANK")) {
            netbankView.show();
        } else if (request.equalsIgnoreCase("PHONE")) {
            phoneView.show();
        } else if (request.equalsIgnoreCase("EMAIL")) {
            emailView.show();
        } else if (request.equalsIgnoreCase("WEBSITE")) {
            websiteView.show();
        } else if (request.equalsIgnoreCase("OTHER")) {
            otherView.show();
        }
    }

    public void close(String request) {
        if (request.equalsIgnoreCase("MAINFRAME")) {
            mainFrameView.close();
        } else if (request.equalsIgnoreCase("BANK_ACCOUNT")) {
            bankAccountView.close();
        } else if (request.equalsIgnoreCase("PERSONAL_FORM")) {
            personalView.close();
        } else if (request.equalsIgnoreCase("SETTINGS_FORM")) {
            settingsView.close();
        } else if (request.equalsIgnoreCase("FILE")) {
            fileView.close();
        } else if (request.equalsIgnoreCase("BANK_CARD")) {
            bankCardView.close();
        } else if (request.equalsIgnoreCase("NETBANK")) {
            netbankView.close();
        } else if (request.equalsIgnoreCase("PHONE")) {
            phoneView.close();
        } else if (request.equalsIgnoreCase("EMAIL")) {
            emailView.close();
        } else if (request.equalsIgnoreCase("WEBSITE")) {
            websiteView.close();
        } else if (request.equalsIgnoreCase("OTHER")) {
            otherView.close();
        }
    }
}
