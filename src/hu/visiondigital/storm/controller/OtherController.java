package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class OtherController extends EntityController implements Initializable, EntityCreator<Other>, Validator {

    @FXML
    private Label name, password, description, identifier;
    @FXML
    private TextField nameField, identifierField;
    @FXML
    private PasswordGroup passwordField;
    @FXML
    private TextArea descriptionField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        Other selected = (Other) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        }
        try {
            persist(selected);
            close(Categories.OTHER);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        close(Categories.OTHER);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initFields();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        name.setText(bundle.getString("label.name"));
        password.setText(bundle.getString("label.password"));
        description.setText(bundle.getString("label.description"));
        identifier.setText(bundle.getString("label.identifier"));
        message.setText(baseBundle.getString("error.message"));
    }

    private void initFields() {
        if (SessionHelper.getInstance().getValue("selected") != null) {
            Other selected = (Other) SessionHelper.getInstance().getValue("selected");
            nameField.textProperty().bindBidirectional(selected.nameProperty());
            passwordField.textProperty().bindBidirectional(selected.passwordProperty());
            descriptionField.textProperty().bindBidirectional(selected.descriptionProperty());
            identifierField.textProperty().bindBidirectional(selected.identifierProperty());
        }
    }

    @Override
    public Other createEntity() {
        if (isValid()) {
            Other entity = new Other();
            entity.setName(nameField.getText());
            entity.setPassword(passwordField.getText());
            entity.setDescription(descriptionField.getText());
            entity.setIdentifier(identifierField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !nameField.getText().trim().equals("");
    }

}
