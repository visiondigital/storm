package hu.visiondigital.storm.controller;

import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;
import javafx.application.Platform;

/**
 *
 * @author belakede
 */
abstract class CategoryController extends AbstractController {

    protected List<Button> categories;

    @FXML
    protected Button settings, files, bankaccount, creditcard, netbank, phone,
            emailaccount, websites, other, newpassword, quit;
    @FXML
    protected TableView tableView;

    @FXML
    protected void loadPersonal(ActionEvent event) {
        loadSpecial("PERSONAL_FORM");
    }

    @FXML
    protected void loadSettings(ActionEvent event) {
        loadSpecial("SETTINGS_FORM");
    }

    @FXML
    protected void loadFile(ActionEvent event) {
        load(Categories.FILE, files, new File(), "file");
    }

    @FXML
    protected void loadBankaccount(ActionEvent event) {
        load(Categories.BANK_ACCOUNT, bankaccount, new BankAccount(), "bankaccount");
    }

    @FXML
    protected void loadBankcard(ActionEvent event) {
        load(Categories.BANK_CARD, creditcard, new BankCard(), "bankcard");
    }

    @FXML
    protected void loadNetbank(ActionEvent event) {
        load(Categories.NETBANK, netbank, new Netbank(), "netbank");
    }

    @FXML
    protected void loadPhone(ActionEvent event) {
        load(Categories.PHONE, phone, new Phone(), "phone");
    }

    @FXML
    protected void loadEmail(ActionEvent event) {
        load(Categories.EMAIL, emailaccount, new Email(), "email");
    }

    @FXML
    protected void loadWebsite(ActionEvent event) {
        load(Categories.WEBSITE, websites, new Website(), "website");
    }

    @FXML
    protected void loadOther(ActionEvent event) {
        load(Categories.OTHER, other, new Other(), "other");
    }

    private void load(Categories type, Button source, EntityWithId entity, String bundleName) {
        setAsActive(type, source);
        TableHelper.setupTable(tableView, entity, bundleName);
    }

    private void loadSpecial(String request) {
        FrontController controller = FrontController.getInstance(null, null);
        controller.dispatchRequest(request);
    }

    private void setAsActive(Categories type, Button source) {
        changeActiveStyleClass(source);
        SessionHelper.getInstance().setValue("type", type);
    }

    private void changeActiveStyleClass(final Button source) {
        Platform.runLater(() -> {
            categories.stream().forEach((Button button) -> {
                button.getStyleClass().removeAll("active");
            });
            source.getStyleClass().add("active");
        });
    }
    abstract protected void init();

    protected void load(Categories category) {
        switch (category) {
            case BANK_ACCOUNT:
                loadBankaccount(null);
                break;
            case BANK_CARD:
                loadBankcard(null);
                break;
            case EMAIL:
                loadEmail(null);
                break;
            case FILE:
                loadFile(null);
                break;
            case NETBANK:
                loadNetbank(null);
                break;
            case OTHER:
                loadOther(null);
                break;
            case PHONE:
                loadPhone(null);
                break;
            case WEBSITE:
                loadWebsite(null);
                break;
        }
    }

}
