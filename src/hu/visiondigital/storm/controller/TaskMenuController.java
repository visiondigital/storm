package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;

/**
 * FXML Controller class
 *
 * @author belakede
 */
public class TaskMenuController implements Initializable {

    @FXML
    private Button close;
    private ResourceBundle bundle;
    
    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        this.bundle = bundle;
        initTexts();
    }
    
    private void initTexts() {
        close.setText(bundle.getString("button.exit"));
    }
    
    @FXML
    private void closeApplication(ActionEvent event) {
        System.exit(0);
    }

}
