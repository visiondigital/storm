package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class BankAccountController extends EntityController implements Initializable, EntityCreator<BankAccount>, Validator {

    @FXML
    private Label owner, bank, accountnumber, iban, securitycode;
    @FXML
    private TextField ownerField, bankField, accountnumberField, ibanField;
    @FXML
    private PasswordGroup securitycodeField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        BankAccount selected = (BankAccount) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        }
        try {
            persist(selected);
            close(Categories.BANK_ACCOUNT);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    protected void cancelButtonAction(ActionEvent event) {
        close(Categories.BANK_ACCOUNT);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initFields();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        owner.setText(bundle.getString("label.owner"));
        bank.setText(bundle.getString("label.bank"));
        accountnumber.setText(bundle.getString("label.accountnumber"));
        iban.setText(bundle.getString("label.iban"));
        securitycode.setText(bundle.getString("label.securitycode"));
        message.setText(baseBundle.getString("error.message"));
    }

    private void initFields() {
        if (SessionHelper.getInstance().getValue("selected") != null) {
            BankAccount selected = (BankAccount) SessionHelper.getInstance().getValue("selected");
            ownerField.textProperty().bindBidirectional(selected.ownerProperty());
            bankField.textProperty().bindBidirectional(selected.bankProperty());
            accountnumberField.textProperty().bindBidirectional(selected.accountNumberProperty());
            ibanField.textProperty().bindBidirectional(selected.ibanProperty());
            securitycodeField.textProperty().bindBidirectional(selected.securityCodeProperty());
        }
    }

    @Override
    public BankAccount createEntity() {
        if (isValid()) {
            BankAccount entity = new BankAccount();
            entity.setOwner(ownerField.getText());
            entity.setBank(bankField.getText());
            entity.setAccountNumber(accountnumberField.getText());
            entity.setIban(ibanField.getText());
            entity.setSecurityCode(securitycodeField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !ownerField.getText().trim().equals("")
                && !bankField.getText().trim().equals("")
                && !accountnumberField.getText().trim().equals("");
    }

}
