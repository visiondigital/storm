package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class EmailController extends EntityController implements Initializable, EntityCreator<Email>, Validator {

    @FXML
    private Label username, address, password, description;
    @FXML
    private TextField usernameField, hostField, addressField, descriptionField;
    @FXML
    private PasswordGroup passwordField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        Email selected = (Email) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        } else {
            selected.setPassword(selected.passwordProperty().get());
        }
        try {
            persist(selected);
            close(Categories.EMAIL);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    protected void cancelButtonAction(ActionEvent event) {
        close(Categories.EMAIL);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initFields();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        username.setText(bundle.getString("label.username"));
        address.setText(bundle.getString("label.address"));
        password.setText(bundle.getString("label.password"));
        description.setText(bundle.getString("label.description"));
        message.setText(baseBundle.getString("error.message"));
    }

    private void initFields() {
        if (SessionHelper.getInstance().getValue("selected") != null) {
            Email selected = (Email) SessionHelper.getInstance().getValue("selected");
            usernameField.textProperty().bindBidirectional(selected.usernameProperty());
            hostField.textProperty().bindBidirectional(selected.hostProperty());
            addressField.textProperty().bindBidirectional(selected.addressProperty());
            passwordField.textProperty().bindBidirectional(selected.passwordProperty());
            descriptionField.textProperty().bindBidirectional(selected.descriptionProperty());
        }
    }

    @Override
    public Email createEntity() {
        if (isValid()) {
            Email entity = new Email();
            entity.setUsername(usernameField.getText());
            entity.setHost(hostField.getText());
            entity.setAddress(addressField.getText());
            entity.setPassword(passwordField.getText());
            entity.setDescription(descriptionField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !addressField.getText().trim().equals("")
                && !passwordField.getText().trim().equals("");
    }

}
