package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class NetbankController extends EntityController implements Initializable, EntityCreator<Netbank>, Validator {

    @FXML
    private Label username, password, url, phone;
    @FXML
    private TextField usernameField, urlField, phoneField;
    @FXML
    private PasswordGroup passwordField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        Netbank selected = (Netbank) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        }
        try {
            persist(selected);
            close(Categories.NETBANK);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        close(Categories.NETBANK);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initFields();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        username.setText(bundle.getString("label.username"));
        password.setText(bundle.getString("label.password"));
        url.setText(bundle.getString("label.url"));
        phone.setText(bundle.getString("label.phone"));
        message.setText(baseBundle.getString("error.message"));
    }
    
    private void initFields() {
        if (SessionHelper.getInstance().getValue("selected") != null) {
            Netbank selected = (Netbank) SessionHelper.getInstance().getValue("selected");
            usernameField.textProperty().bindBidirectional(selected.usernameProperty());
            passwordField.textProperty().bindBidirectional(selected.passwordProperty());
            urlField.textProperty().bindBidirectional(selected.urlProperty());
            phoneField.textProperty().bindBidirectional(selected.phoneProperty());
        }
    }

    @Override
    public Netbank createEntity() {
        if (isValid()) {
            Netbank entity = new Netbank();
            entity.setUsername(usernameField.getText());
            entity.setPassword(passwordField.getText());
            entity.setUrl(urlField.getText());
            entity.setPhone(phoneField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !usernameField.getText().trim().equals("")
                && !passwordField.getText().trim().equals("");
    }

}
