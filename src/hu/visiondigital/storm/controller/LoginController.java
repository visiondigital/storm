package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;

/**
 *
 * @author belakede
 */
public class LoginController extends AbstractController implements Initializable {

    @FXML
    private Label title, message;
    @FXML
    private Button close, unlock;
    @FXML
    private PasswordField passwordField;

    @FXML
    private void closeButtonAction(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void unlockButtonAction(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();

        Settings settings = SessionHelper.getInstance().getSettings();
        String pinCode = EncryptionHelper.getInstance().hash(passwordField.getText().trim());
        if (pinCode.equals(settings.getPinCode())) {
            SessionHelper.getInstance().loggedInProperty().set(true);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        message.setText(bundle.getString("label.message"));
        close.setText(bundle.getString("button.close"));
        unlock.setText(bundle.getString("button.unlock"));
    }

}
