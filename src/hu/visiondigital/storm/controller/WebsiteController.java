package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class WebsiteController extends EntityController implements Initializable, EntityCreator<Website>, Validator {

    @FXML
    private Label name, url, username, email, password, description;
    @FXML
    private TextField nameField, urlField, usernameField, emailField;
    @FXML
    private PasswordGroup passwordField;
    @FXML
    private TextArea descriptionField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        Website selected = (Website) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        } else {
            selected.setPassword(selected.passwordProperty().get());
        }
        try {
            persist(selected);
            close(Categories.WEBSITE);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        close(Categories.WEBSITE);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initFields();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        name.setText(bundle.getString("label.name"));
        url.setText(bundle.getString("label.url"));
        username.setText(bundle.getString("label.username"));
        email.setText(bundle.getString("label.email"));
        password.setText(bundle.getString("label.password"));
        description.setText(bundle.getString("label.description"));
        message.setText(baseBundle.getString("error.message"));
    }

    private void initFields() {
        if (SessionHelper.getInstance().getValue("selected") != null) {
            Website selected = (Website) SessionHelper.getInstance().getValue("selected");
            nameField.textProperty().bindBidirectional(selected.nameProperty());
            urlField.textProperty().bindBidirectional(selected.urlProperty());
            usernameField.textProperty().bindBidirectional(selected.usernameProperty());
            emailField.textProperty().bindBidirectional(selected.emailProperty());
            passwordField.textProperty().bindBidirectional(selected.passwordProperty());
            descriptionField.textProperty().bindBidirectional(selected.descriptionProperty());
        }
    }

    @Override
    public Website createEntity() {
        if (isValid()) {
            Website entity = new Website();
            entity.setName(nameField.getText());
            entity.setUrl(urlField.getText());
            entity.setUsername(usernameField.getText());
            entity.setEmail(emailField.getText());
            entity.setPassword(passwordField.getText());
            entity.setDescription(descriptionField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !urlField.getText().trim().equals("")
                && !passwordField.getText().trim().equals("");
    }

}
