package hu.visiondigital.storm.controller;

import javafx.fxml.*;
import javafx.util.*;
import javafx.event.*;
import javafx.animation.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;

/**
 *
 * @author belakede
 */
public class EntityController extends AbstractController {

    @FXML
    protected Button cancel;
    @FXML
    protected Button add;
    @FXML
    protected Label title;
    @FXML
    protected LabelWithFontIcon message;


    protected void close(Categories category) {
        FrontController controller = FrontController.getInstance(null, null);
        controller.closeRequest(category.name());
    }

    protected void persist(EntityWithId entity) throws DatabaseException {
        if (entity == null) {
            throw new DatabaseException("Entity is null...");
        }
        if (entity.getId() != null && !entity.getId().equals(0)) {
            DatabaseHelper.getInstance().getManager(entity).update(entity);
        } else {
            DatabaseHelper.getInstance().getManager(entity).create(entity);
        }
        SessionHelper.getInstance().setValue("selected", entity);
    }

    protected void showMessage() {
        message.setVisible(true);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), (ActionEvent event) -> {
            message.setVisible(false);
        }));
        timeline.setDelay(Duration.seconds(6));
        timeline.play();
    }

}
