package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class FileController extends EntityController implements Initializable, EntityCreator<File>, Validator {

    @FXML
    private Label filename, md5sum, password, description;
    @FXML
    private TextField filenameField, md5sumField;
    @FXML
    private PasswordGroup passwordField;
    @FXML
    private TextArea descriptionField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        File selected = (File) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        } else {
            selected.setPassword(selected.passwordProperty().get());
        }
        try {
            persist(selected);
            close(Categories.FILE);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        close(Categories.FILE);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initField();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        filename.setText(bundle.getString("label.filename"));
        md5sum.setText(bundle.getString("label.md5sum"));
        password.setText(bundle.getString("label.password"));
        description.setText(bundle.getString("label.description"));
        message.setText(baseBundle.getString("error.message"));
    }

    private void initField() {
        if (SessionHelper.getInstance().getValue("selected") != null) {
            File selected = (File) SessionHelper.getInstance().getValue("selected");
            filenameField.textProperty().bindBidirectional(selected.filenameProperty());
            md5sumField.textProperty().bindBidirectional(selected.md5sumProperty());
            passwordField.textProperty().bindBidirectional(selected.passwordProperty());
            descriptionField.textProperty().bindBidirectional(selected.descriptionProperty());
        }
    }

    @Override
    public File createEntity() {
        if (isValid()) {
            File entity = new File();
            entity.setFilename(filenameField.getText());
            entity.setMd5sum(md5sumField.getText());
            entity.setPassword(passwordField.getText());
            entity.setDescription(descriptionField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !filenameField.getText().trim().equals("")
                && !passwordField.getText().trim().equals("");
    }

}
