package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;

/**
 *
 * @author belakede
 */
public class RegistrationController extends AbstractController implements Initializable {

    @FXML
    private Label title, message, username, pincode;
    @FXML
    private Button close, unlock;
    @FXML
    private PasswordGroup pincodeField;
    @FXML
    private TextField nicknameField;

    @FXML
    private void closeButtonAction(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void unlockButtonAction(ActionEvent event) {
        Settings settings = SessionHelper.getInstance().getSettings();
        Personal personal = SessionHelper.getInstance().getPersonal();
        settings.setPinCode(EncryptionHelper.getInstance().hash(pincodeField.getText().trim()));
        personal.setNickname(nicknameField.getText().trim());

        if (SessionHelper.getInstance().isFirstRun()) {
            if (((settings.getPinCode() != null && personal.getNickname() != null))
                    && (!settings.getPinCode().isEmpty() && !personal.getNickname().isEmpty())) {
                SessionHelper.getInstance().register(settings, personal);
            }
        }

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        message.setText(bundle.getString("label.message"));
        username.setText(bundle.getString("label.username"));
        pincode.setText(bundle.getString("label.pincode"));
        close.setText(bundle.getString("button.close"));
        unlock.setText(bundle.getString("button.unlock"));
    }

}
