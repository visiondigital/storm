package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import java.beans.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.beans.value.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.model.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;

/**
 *
 * @author belakede
 */
public class SettingsController extends AbstractController implements Initializable, PropertyChangeListener {

    @FXML
    private Label title, pincode, theme, language, defaultcategory, pinlimit;
    @FXML
    private Button cancel, add;
    @FXML
    private PasswordGroup pincodeField;
    @FXML
    private ThemeChooser themeField;
    @FXML
    private ComboBox<Locales> languageField;
    @FXML
    private ComboBox<Categories> categoryField;
    @FXML
    private Slider pinlimitField;
    @FXML
    private CheckBox hideonexitField, showonsystrayField;

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        Settings settings = SessionHelper.getInstance().getSettings();
        DatabaseHelper.getInstance().getSettingsManager().read(settings.getId());
        close();
    }

    @FXML
    private void addButtonAction(ActionEvent event) {
        Settings settings = SessionHelper.getInstance().getSettings();
        if (settings.getPinCode().length() < 12) {
            settings.setPinCode(EncryptionHelper.getInstance().hash(pincodeField.getText().trim()));
        }
        if (settings.getPinLimit() < 1) {
            settings.pinLimitProperty().set(1);
        }
        DatabaseHelper.getInstance().getSettingsManager().update(settings);
        close();
    }

    private void close() {
        FrontController controller = FrontController.getInstance(null, null);
        controller.closeRequest("SETTINGS_FORM");
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        register();
        init();
        initFields();
    }

    private void register() {
        GlobalPropertyChangeSupport.getInstance().registerPropertyChageListener(this);
    }

    private void unregister() {
        GlobalPropertyChangeSupport.getInstance().unregisterPropertyChageListener(this);
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        pincode.setText(bundle.getString("label.pincode"));
        theme.setText(bundle.getString("label.theme"));
        language.setText(bundle.getString("label.language"));
        defaultcategory.setText(bundle.getString("label.defaultcategory"));
        pinlimit.setText(bundle.getString("label.pinlimit"));
    }

    private void initFields() {
        categoryField.getItems().addAll(Categories.values());
        languageField.getItems().addAll(Locales.values());

        Settings settings = SessionHelper.getInstance().getSettings();

        pincodeField.textProperty().bindBidirectional(settings.pinCodeProperty());
        themeField.valueProperty().bindBidirectional(settings.themeProperty());
        languageField.valueProperty().bindBidirectional(settings.languageProperty());
        categoryField.valueProperty().bindBidirectional(settings.defaultCategoryProperty());
        pinlimitField.valueProperty().bindBidirectional(settings.pinLimitProperty());
        hideonexitField.selectedProperty().bindBidirectional(settings.hideOnExitProperty());
        showonsystrayField.selectedProperty().bindBidirectional(settings.showOnSystrayProperty());

        showonsystrayField.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                hideonexitField.selectedProperty().set(false);
            }
        });
        hideonexitField.disableProperty().bind(showonsystrayField.selectedProperty().not());
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event != null && event.getSource() instanceof Manageable && "read".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(Settings.class)) {
                Settings settings = SessionHelper.getInstance().getSettings();
                Settings read = (Settings) event.getNewValue();
                pincodeField.setText(read.getPinCode());
                languageField.getSelectionModel().select(read.getLanguage());
                categoryField.getSelectionModel().select(read.getDefaultCategory());
                pinlimitField.setValue(read.getPinLimit());
                hideonexitField.selectedProperty().set(read.isHideOnExit());
                showonsystrayField.selectedProperty().set(read.isShowOnSystray());
                unregister();
            }
        }
    }

}
