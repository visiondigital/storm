package hu.visiondigital.storm.controller.interfaces;

import hu.visiondigital.storm.model.entity.*;

/**
 * Summarizes controller classes that create entity instances.
 * @author timea
 * @param <T> 
 * @see EntityWithId
 */
public interface EntityCreator<T extends EntityWithId> {

    /**
     * Creates an entity instance of the entity class passed through the type parameter.
     * @return T
     */
    public T createEntity();
}
