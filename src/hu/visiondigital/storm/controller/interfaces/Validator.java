package hu.visiondigital.storm.controller.interfaces;

/**
 * A class implements Validator to validate forms.
 * @author juuci
 */
public interface Validator {

    /**
     * Decides if a field of a form is valid.
     * @return boolean
     */
    public boolean isValid();
}
