package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import java.beans.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.model.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class BankCardController extends EntityController implements Initializable, EntityCreator<BankCard>, Validator, PropertyChangeListener {

    @FXML
    private Label bankaccount, cardnumber, expirationdate, verificationcode, owner, cardtype, disablenumber;
    @FXML
    private TextField cardnumberField, expirationdateField, ownerField, disablenumberField;
    @FXML
    private ComboBox<BankAccount> bankaccountField;
    @FXML
    private ComboBox<CardType> cardtypeField;
    @FXML
    private PasswordGroup verificationcodeField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        BankCard selected = (BankCard) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        }
        try {
            persist(selected);
            close(Categories.BANK_CARD);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        close(Categories.BANK_CARD);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        register();
        init();
        initFields();
    }

    private void register() {
        GlobalPropertyChangeSupport.getInstance().registerPropertyChageListener(this);
    }

    private void unregister() {
        GlobalPropertyChangeSupport.getInstance().unregisterPropertyChageListener(this);
    }

    private void init() {
        DatabaseHelper.getInstance().getBankAccountManager().readAll();

        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        bankaccount.setText(bundle.getString("label.bankaccount"));
        cardnumber.setText(bundle.getString("label.cardnumber"));
        expirationdate.setText(bundle.getString("label.expirationdate"));
        verificationcode.setText(bundle.getString("label.verificationcode"));
        owner.setText(bundle.getString("label.owner"));
        cardtype.setText(bundle.getString("label.cardtype"));
        disablenumber.setText(bundle.getString("label.disablenumber"));
        message.setText(baseBundle.getString("error.message"));
    }

    private void initFields() {
        cardtypeField.getItems().addAll(CardType.values());

        if (SessionHelper.getInstance().getValue("selected") != null) {
            BankCard selected = (BankCard) SessionHelper.getInstance().getValue("selected");
            bankaccountField.valueProperty().bindBidirectional(selected.bankAccountProperty());
            cardnumberField.textProperty().bindBidirectional(selected.cardNumberProperty());
            expirationdateField.textProperty().bindBidirectional(selected.expirationDateProperty());
            verificationcodeField.textProperty().bindBidirectional(selected.verificationCodeProperty());
            ownerField.textProperty().bindBidirectional(selected.ownerProperty());
            cardtypeField.valueProperty().bindBidirectional(selected.cardTypeProperty());
            disablenumberField.textProperty().bindBidirectional(selected.disableNumberProperty());
        }
    }

    @Override
    public BankCard createEntity() {
        if (isValid()) {
            BankCard entity = new BankCard();
            entity.setBankAccount((BankAccount) bankaccountField.getSelectionModel().getSelectedItem());
            entity.setCardNumber(cardnumberField.getText());
            entity.setExpirationDate(expirationdateField.getText());
            entity.setVerificationCode(verificationcodeField.getText());
            entity.setOwner(ownerField.getText());
            entity.setCardType((CardType) cardtypeField.getSelectionModel().getSelectedItem());
            entity.setDisableNumber(disablenumberField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !bankaccountField.getSelectionModel().isEmpty()
                && !cardnumberField.getText().trim().equals("")
                && !expirationdateField.getText().trim().equals("")
                && !verificationcodeField.getText().trim().equals("")
                && !ownerField.getText().trim().equals("");
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event != null && event.getSource() instanceof Manageable && "readAll".equals(event.getPropertyName())) {
            Manageable manager = (Manageable) event.getSource();
            if (manager.getType().equals(BankAccount.class)) {
                List<BankAccount> items = (List<BankAccount>) event.getNewValue();
                bankaccountField.getItems().addAll(items);
                unregister();
            }
        }
    }

}
