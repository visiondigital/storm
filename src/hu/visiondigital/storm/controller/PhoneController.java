package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.enums.*;
import hu.visiondigital.storm.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;
import hu.visiondigital.storm.model.exceptions.*;
import hu.visiondigital.storm.controller.interfaces.*;

/**
 *
 * @author belakede
 */
public class PhoneController extends EntityController implements Initializable, EntityCreator<Phone>, Validator {

    @FXML
    private Label number, pin, puk, securitycode, provider;
    @FXML
    private TextField numberField, providerField;
    @FXML
    private PasswordGroup pinField, pukField, securitycodeField;

    @FXML
    private void addButtonAction(ActionEvent event) {
        Phone selected = (Phone) SessionHelper.getInstance().getValue("selected");
        if (selected == null) {
            selected = createEntity();
        }
        try {
            persist(selected);
            close(Categories.PHONE);
        } catch (DatabaseException ex) {
            message.setText(baseBundle.getString("error.message"));
            showMessage();
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        close(Categories.PHONE);
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initFields();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        number.setText(bundle.getString("label.number"));
        pin.setText(bundle.getString("label.pin"));
        puk.setText(bundle.getString("label.puk"));
        securitycode.setText(bundle.getString("label.securitycode"));
        provider.setText(bundle.getString("label.provider"));
        message.setText(baseBundle.getString("error.message"));
    }

    private void initFields() {
        if (SessionHelper.getInstance().getValue("selected") != null) {
            Phone selected = (Phone) SessionHelper.getInstance().getValue("selected");
            numberField.textProperty().bindBidirectional(selected.numberProperty());
            pinField.textProperty().bindBidirectional(selected.pinProperty());
            pukField.textProperty().bindBidirectional(selected.pukProperty());
            securitycodeField.textProperty().bindBidirectional(selected.securityCodeProperty());
            providerField.textProperty().bindBidirectional(selected.providerProperty());
        }
    }

    @Override
    public Phone createEntity() {
        if (isValid()) {
            Phone entity = new Phone();
            entity.setNumber(numberField.getText());
            entity.setPin(pinField.getText());
            entity.setPuk(pukField.getText());
            entity.setSecurityCode(securitycodeField.getText());
            entity.setProvider(providerField.getText());
            return entity;
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        return !numberField.getText().trim().equals("")
                && !pinField.getText().trim().equals("");
    }

}
