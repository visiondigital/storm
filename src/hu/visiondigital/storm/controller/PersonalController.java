package hu.visiondigital.storm.controller;

import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.event.*;
import javafx.scene.control.*;
import hu.visiondigital.storm.helpers.*;
import hu.visiondigital.storm.model.entity.*;

/**
 *
 * @author belakede
 */
public class PersonalController extends AbstractController implements Initializable {

    @FXML
    private Button cancel, add;
    @FXML
    private Label title, fullname, nickname, idcard, studentcard, nationalid,
            taxid, socialsecurity, drivinglicense, passport, educationid;
    @FXML
    private TextField fullnameField, nicknameField, idcardField, studentcardField,
            nationalidField, taxidField, socialsecurityField, drivinglicenseField,
            passportField, educationidField;

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        DatabaseHelper.getInstance().getPersonalManager().read(SessionHelper.getInstance().getPersonal().getId());
        close();
    }

    @FXML
    private void addButtonAction(ActionEvent event) {
        DatabaseHelper.getInstance().getPersonalManager().update(SessionHelper.getInstance().getPersonal());
        close();
    }

    public void close() {
        FrontController controller = FrontController.getInstance(null, null);
        controller.closeRequest("PERSONAL_FORM");
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        setResourceBundle(bundle);
        init();
        initFields();
    }

    private void init() {
        title.setText(bundle.getString("label.title"));
        cancel.setText(bundle.getString("button.cancel"));
        add.setText(bundle.getString("button.add"));
        fullname.setText(bundle.getString("label.fullname"));
        nickname.setText(bundle.getString("label.nickname"));
        idcard.setText(bundle.getString("label.idcard"));
        studentcard.setText(bundle.getString("label.studentcard"));
        nationalid.setText(bundle.getString("label.nationalid"));
        taxid.setText(bundle.getString("label.taxid"));
        socialsecurity.setText(bundle.getString("label.socialsecurity"));
        drivinglicense.setText(bundle.getString("label.drivinglicense"));
        passport.setText(bundle.getString("label.passport"));
        educationid.setText(bundle.getString("label.educationid"));
    }

    private void initFields() {
        Personal personal = SessionHelper.getInstance().getPersonal();

        fullnameField.textProperty().bindBidirectional(personal.fullNameProperty());
        nicknameField.textProperty().bindBidirectional(personal.nicknameProperty());
        idcardField.textProperty().bindBidirectional(personal.idCardNoProperty());
        studentcardField.textProperty().bindBidirectional(personal.studentCardNoProperty());
        nationalidField.textProperty().bindBidirectional(personal.nationalIdNoProperty());
        taxidField.textProperty().bindBidirectional(personal.taxIdNoProperty());
        socialsecurityField.textProperty().bindBidirectional(personal.socialSecurityNoProperty());
        drivinglicenseField.textProperty().bindBidirectional(personal.drivingLicenceNoProperty());
        passportField.textProperty().bindBidirectional(personal.passportNoProperty());
        educationidField.textProperty().bindBidirectional(personal.educationIdNoProperty());

    }

}
