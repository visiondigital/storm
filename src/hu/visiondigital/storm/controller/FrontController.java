package hu.visiondigital.storm.controller;

import javafx.event.*;
import javafx.stage.*;
import javafx.concurrent.*;
import javafx.scene.layout.*;
import hu.visiondigital.storm.view.*;
import hu.visiondigital.storm.helpers.*;

/**
 *
 * @author juuci
 */
public class FrontController {

    private static FrontController instance;

    public static FrontController getInstance(Stage stage, StackPane root) {
        if (instance == null) {
            instance = new FrontController(stage, root);
        }
        return instance;
    }

    private final Dispatcher dispatcher;
    private final StackPane root;
    private final Stage stage;

    private FrontController(Stage stage, StackPane root) {
        this.stage = stage;
        this.root = root;
        dispatcher = new Dispatcher(root);
    }

    public boolean isAuthenticUser() {
        while (!SessionHelper.getInstance().isLoggedIn()) {
            new LoginView().show();
        };
        return true;
    }

    public void dispatchRequest(String request) {
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                registration();
                return null;
            }
        };
        task.setOnSucceeded((Event event) -> {
            dispatch(request);
        });
        task.run();
    }

    private void registration() {
        while (!isUserRegistered()) {
            new RegistrationView().show();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
        }
    }

    private void dispatch(String request) {
        if (isAuthenticUser()) {
            dispatcher.dispatch(request);
            stage.show();
        }
    }

    public void closeRequest(String request) {
        if (isAuthenticUser()) {
            dispatcher.close(request);
        }
    }

    public boolean isUserRegistered() {
        return !SessionHelper.getInstance().isFirstRun();
    }
}
