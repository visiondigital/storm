package hu.visiondigital.storm.controller;

import java.util.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.input.*;
import hu.visiondigital.storm.helpers.*;

/**
 *
 * @author belakede
 */
public class AbstractController {

    protected ResourceBundle bundle;
    protected ResourceBundle baseBundle;
    protected double startX;
    protected double startY;

    protected void setResourceBundle(ResourceBundle bundle) {
        this.bundle = bundle;
        this.baseBundle = ResourceBundle.getBundle("hu.visiondigital.storm.resources.i18n.base", SessionHelper.getInstance().getSettings().getLanguage().locale());
    }

    @FXML
    protected void touch(MouseEvent event) throws Exception {
        Node source = (Node) event.getSource();
        source.setCursor(Cursor.MOVE);
        if (event.getButton() != MouseButton.MIDDLE) {
            startX = event.getSceneX();
            startY = event.getSceneY();
        }
    }

    @FXML
    protected void move(MouseEvent event) throws Exception {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        if (event.getButton() != MouseButton.MIDDLE) {
            stage.setX(event.getScreenX() - startX);
            stage.setY(event.getScreenY() - startY);
        }
    }
    
}
